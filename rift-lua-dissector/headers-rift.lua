-- 
-- Lua dissector for RIFT headers that transmit data over UDP 
-- The payload is later interpreted as serialized RIFT model object (requires rift.lua dissector)
--

-- declare the protocol
rifth_proto = Proto("RIFT-headers","RIFT headers over UDP")

-- The offset of the headers that will be skipped 
local OUTER_HEADER=20
local TIE_ORIGIN_HEADER=8 
local TOTAL_HEADERS=OUTER_HEADER+TIE_ORIGIN_HEADER
local ERR_WRONG_MAGIC = "Wrong magic value"

-- Create a simple dissection function
function rifth_proto.dissector(buffer, pinfo, tree)

   -- Create the rifth tree
  local t_rifth = tree:add(rifth_proto, buffer())
  local frame_len = buffer:len()
  local offset = 0
  local rift_magic = buffer(offset,2):uint()
  local new_buffer

  if rift_magic == 0xa1f7 then
    t_rifth:add(buffer(offset,2), "RIFT MAGIC: ", rift_magic)
    offset=offset+2
    local seq = buffer(offset,2):uint()
    t_rifth:add(buffer(offset,2), "Packet Number: ", seq)
    offset=offset+2
    local version = buffer(offset+1,1):uint()
    t_rifth:add(buffer(offset+1,1), "RIFT Major Version: ", version)
    offset=offset+2
    local tie_ttl = buffer(offset+6,4):uint()
    if tie_ttl ~= 0xffffffff then
      t_rifth:add(buffer(offset+6,4), "TIE TTL: ", tie_ttl)
      offset=offset+10

      -- Skip the uninterpreted rifth header data
      t_rifth:add(buffer(offset,TOTAL_HEADERS-offset-4),"Header:", "Uninterpreted Data Sequence ("..TOTAL_HEADERS-offset.." bytes)")

      -- prepare the rest of the buffer. As packet is a TIE we must skip the two headers
      new_buffer = buffer(TOTAL_HEADERS-4,frame_len-28+4)
    else
      -- Skip the uninterpreted rifth header data and prepare the rest of the buffer.
      t_rifth:add(buffer(offset,OUTER_HEADER-offset-4),"Header:", "Uninterpreted Data Sequence ("..OUTER_HEADER-offset.." bytes)")
      new_buffer = buffer(OUTER_HEADER-4,frame_len-20+4)
    end

    -- Call RIFT dissector on the rest of the buffer (serialized RIFT model object)
    local dissector = Dissector.get("rift")
    dissector:call(new_buffer:tvb(),pinfo,tree)
    pinfo.cols['protocol'] = "RIFT"

  else
    t_rifth:add(buffer(offset,2), "RIFT MAGIC: ", ERR_WRONG_MAGIC)
  end
end


 -- load the udp port table
udp_table = DissectorTable.get("udp.port")
-- register the protocol to ports 10000,10001 ...
udp_table:add(10000, rifth_proto)
udp_table:add(10001, rifth_proto)