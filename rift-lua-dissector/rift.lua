-- 
-- Lua dissector for RIFT packets that transmit data over UDP 
--

-- declare the protocol
rift_proto = Proto("RIFT","Serialized RIFT model object")

-- Create a simple dissection function
function rift_proto.dissector(buffer, pinfo, tree)

    -- Create the rift tree
     local t_rift = tree:add(rift_proto, buffer())
     local frame_len = buffer:len()
     local offset = 0

 end

-- typedef i64      SystemIDType
-- typedef i32      IPv4Address
-- /** this has to be long enough to accomodate prefix */
-- typedef binary   IPv6Address
-- /** @note MUST be interpreted in implementation as unsigned */
-- typedef i16      UDPPortType
-- /** @note MUST be interpreted in implementation as unsigned */
-- typedef i32      TIENrType
-- /** @note MUST be interpreted in implementation as unsigned */
-- typedef i32      MTUSizeType
-- /** @note MUST be interpreted in implementation as unsigned
--     rolling over number */
-- typedef i16      SeqNrType
-- /** @note MUST be interpreted in implementation as unsigned */
-- typedef i32      LifeTimeInSecType
-- /** @note MUST be interpreted in implementation as unsigned */
-- typedef i8       LevelType
-- /** optional, recommended monotonically increasing number
-- _per packet type per adjacency_
-- that can be used to detect losses/misordering/restarts. @note MUST be interpreted in implementation as unsigned
--           rolling over number */
-- typedef i16      PacketNumberType
-- /** @note MUST be interpreted in implementation as unsigned */ typedef i32 PodType
-- /** @note MUST be interpreted in implementation as unsigned.
--           This is carried in the
-- security envelope and MUST fit into 8 bits. */ typedef i8 VersionType
-- /** @note MUST be interpreted in implementation as unsigned */
-- typedef i16      MinorVersionType
-- /** @note MUST be interpreted in implementation as unsigned */
-- typedef i32      MetricType
-- /** @note MUST be interpreted in implementation as unsigned
--           and unstructured */
-- typedef i64      RouteTagType
-- /** @note MUST be interpreted in implementation as unstructured
--           label value */
-- typedef i32      LabelType
-- /** @note MUST be interpreted in implementation as unsigned */
-- typedef i32      BandwithInMegaBitsType
-- /** @note Key Value key ID type */
-- typedef string   KeyIDType
-- /** node local, unique identification for a link (interface/tunnel * etc. Basically anything RIFT runs on). This is kept
-- * at 32 bits so it aligns with BFD [RFC5880] discriminator size. */
-- typedef i32    LinkIDType
-- typedef string KeyNameType
-- typedef i8     PrefixLenType
-- /** timestamp in seconds since the epoch */
-- typedef i64    TimestampInSecsType
-- /** security nonce.
-- @note MUST be interpreted in implementation as rolling
--           over unsigned value */
-- typedef i16    NonceType
-- /** LIE FSM holdtime type */
-- typedef i16    TimeIntervalInSecType
-- /** Transaction ID type for prefix mobility as specified by RFC6550,
-- value MUST be interpreted in implementation as unsigned */ typedef i8 PrefixTransactionIDType
-- /** Timestamp per IEEE 802.1AS, all values MUST be interpreted in
-- implementation as unsigned. */
-- struct IEEE802_1ASTimeStampType {
--     1: required i64 AS_sec;
--     2: optional i32 AS_nsec; }
--     /** generic counter type */
--     typedef i64 CounterType
--     /** Platform Interface Index type, i.e. index of interface on hardware,
--     can be used e.g. with RFC5837 */ typedef i32 PlatformInterfaceIndex