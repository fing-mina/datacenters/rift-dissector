/* packet-rift.c
 * Routines for Routing In Fat Trees protocol dissection
 * Copyright 2020, Maximiliano Lucero <maximiliano.martin.lucero.bacardat@fing.edu.uy>
 * Agustina Parnizari <agustina.parnizari@fing.edu.uy>
 *
 * $Id$
 *
 * Wireshark - Network traffic analyzer
 * By Gerald Combs <gerald@wireshark.org>
 * Copyright 1998 Gerald Combs
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

#include "config.h"

#if 0
/* Include only as needed */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#endif

#include <glib.h>

#include <epan/packet.h>
#include <epan/expert.h>
#include <epan/prefs.h>
#include <epan/to_str.h>
#include <inttypes.h>
#include <epan/conversation.h>

#if 0
/* IF AND ONLY IF your protocol dissector exposes code to other dissectors
 * (which most dissectors don't need to do) then the 'public' prototypes and
 * data structures can go in the header file packet-PROTOABBREV.h. If not, then
 * a header file is not needed at all and this #include statement can be
 * removed. */
#include "packet-rift.h"
#endif

void proto_reg_handoff_rift(void);

static dissector_handle_t rift_handle;

/* Initialize the protocol and registered fields */
static int proto_rift = -1;
static int hf_rift_magic = -1;
static int hf_rift_pkt_number = -1;
static int hf_rift_reserved = -1;
static int hf_rift_major_version = -1;
static int hf_rift_outer_key_id = -1;
static int hf_rift_fingerprint_len = -1;
static int hf_rift_fingerprint = -1;
static int hf_rift_weak_nonce_local = -1;
static int hf_rift_weak_nonce_remote = -1;
static int hf_rift_tie_ttl = -1;
static int hf_rift_tie_origin_key_id = -1;
static int hf_rift_tie_fingerprint_len = -1;
static int hf_rift_tie_fingerprint = -1;
static int hf_rift_ser_major_version = -1;
static int hf_rift_ser_minor_version = -1;
static int hf_rift_ser_sender = -1;
static int hf_rift_ser_lvl = -1;
static int hf_rift_pkt_type = -1;
static int hf_rift_tie_direction = -1;
static int hf_rift_tie_originator = -1;
static int hf_rift_tie_elem_type = -1;
static int hf_rift_tie_number = -1;
static int hf_rift_tie_id_seqnum = -1;
static int hf_rift_ieee802_timestamp_sec = -1;
static int hf_rift_ieee802_timestamp_nsec = -1;
static int hf_rift_tie_lifetime = -1;
static int hf_rift_tie_node_level = -1;
static int hf_rift_tie_node_name = -1;
static int hf_rift_tie_node_pod = -1;
static int hf_rift_node_capabilities_minor_version = -1;
static int hf_rift_node_capabilities_flood_reduction = -1;
static int hf_rift_node_capabilities_hierarchy_indications = -1;
static int hf_rift_node_flag_overload = -1;
static int hf_rift_local_link_id_type = -1;
static int hf_rift_remote_link_id_type = -1;
static int hf_rift_platform_if_index = -1;
static int hf_rift_platform_if_name = -1;
static int hf_rift_link_id_sec_key = -1;
static int hf_rift_bfd_up = -1;
static int hf_rift_miscabled_link_id_type = -1;
static int hf_rift_node_neighbor_id = -1;
static int hf_rift_node_neighbor_level = -1;
static int hf_rift_node_neighbor_cost = -1;
static int hf_rift_node_neighbor_bandwidth = -1;
static int hf_rift_tie_prefix_ipv4 = -1;
static int hf_rift_tie_prefix_ipv6 = -1;
static int hf_rift_tie_prefix_len = -1;
static int hf_rift_tie_prefix_attr_metric = -1;
static int hf_rift_tie_prefix_attr_route_tag = -1;
static int hf_rift_tie_prefix_attr_trans_id = -1;
static int hf_rift_tie_prefix_attr_loopback = -1;
static int hf_rift_tie_prefix_attr_dir_attached = -1;
static int hf_rift_tie_prefix_attr_from_link = -1;
static int hf_rift_tie_header_remaining_lifetime = -1;
static int hf_rift_lie_name = -1;
static int hf_rift_lie_flood_port = -1;
static int hf_rift_lie_mtu_size = -1;
static int hf_rift_lie_originator = -1;
static int hf_rift_lie_node_pod = -1;
static int hf_rift_lie_link_bandwidth = -1;
static int hf_rift_link_cap_bfd = -1;
static int hf_rift_link_cap_v4_fwd = -1;
static int hf_rift_lie_adj_holdtime = -1;
static int hf_rift_lie_label_type = -1;
static int hf_rift_lie_not_ztp_offer = -1;
static int hf_rift_lie_flood_repeater = -1;
static int hf_rift_lie_sending_too_quickly = -1;
static int hf_rift_lie_inst_name = -1;
static int hf_rift_tie_key_value_key = -1;
static int hf_rift_tie_key_value_val = -1;

/* Initialize the subtree pointers */
static gint ett_rift = -1;
static gint ett_rift_sec_header = -1;
static gint ett_rift_tie_sec_header = -1;
static gint ett_rift_serialized_obj = -1;
static gint ett_rift_lie = -1;
static gint ett_rift_tire = -1;
static gint ett_rift_tide = -1; 
static gint ett_rift_tie = -1;
static gint ett_rift_tie_node = -1;
static gint ett_rift_tie_prefix = -1;
static gint ett_rift_tie_possitive_diss = -1;
static gint ett_rift_tie_negative_diss = -1;
static gint ett_rift_tie_external = -1;
static gint ett_rift_tie_ext_possitive_diss = -1;
static gint ett_rift_tie_key_value = -1;
static gint ett_rift_tie_key = -1;
static gint ett_rift_tie_neighbors = -1;
static gint ett_rift_tie_neighbor = -1;
static gint ett_rift_tie_link_id_pairs = -1;
static gint ett_rift_tie_link_id_pair = -1;
static gint ett_rift_tie_miscabled_link_id = -1;
static gint ett_rift_tie_prefixes = -1;
static gint ett_rift_tie_prefix_entry = -1;
static gint ett_rift_tie_prefix_attr = -1;
static gint ett_rift_tie_prefix_attr_mon_clock = -1;
static gint ett_rift_tie_prefix_attr_route_tags = -1;
static gint ett_rift_tie_header_with_lifetime = -1;
static gint ett_rift_tieid = -1;
static gint ett_rift_lie_neighbor = -1;
static gint ett_rift_node_cap = -1;
static gint ett_rift_link_cap = -1;

/* Initialize the expert fields */
static expert_field ei_rift_thrift_type_err = EI_INIT;
static expert_field ei_rift_thrift_unexpected_field_id = EI_INIT;
static expert_field ei_rift_tie_dir_err = EI_INIT;
static expert_field ei_rift_tie_elem_err = EI_INIT;

/* A sample #define of the minimum length (in bytes) of the protocol data.
 * If data is received with fewer than this many bytes it is rejected by
 * the current dissector. 
 * NOTE: By now, its just minimum Outer Envelope size (might change later)*/
#define RIFT_MIN_LENGTH 16

/* RIFT Magic Number*/
#define RIFT_MAGIC 0xA1F7
#define NO_TIE_TTL 0xFFFFFFFF

/* Ports */
#define DEFAULT_RIFT_PORT_RANGE "914-915"

/* Needed Thrift flags */
#define THRIFT_BOOL 2
#define THRIFT_BYTE 3
#define THRIFT_I16 6
#define THRIFT_I32 8
#define THRIFT_I64 10
#define THRIFT_STRING 11
#define THRIFT_STRUCT 12
#define THRIFT_MAP 13
#define THRIFT_SET 14
#define THRIFT_LIST 15
#define THRIFT_FIELD_HEADER_LENGTH 3
#define THRIFT_SET_HEADER_LENGTH 5
#define THRIFT_MAP_HEADER_LENGTH 6
#define THRIFT_STRUCT_END 0

/*RIFT Packet Types */
#define LIE_PACKET 1
#define TIDE_PACKET 2
#define TIRE_PACKET 3
#define TIE_PACKET 4

static const value_string riftpkttypevals[] = {
    { LIE_PACKET,    "Link Information Element" },
    { TIDE_PACKET,   "Topology Information Description Element" },
    { TIRE_PACKET,   "Topology Information Request Element" },
    { TIE_PACKET,    "Topology Information Element" },
    { 0, NULL }
};

/* Direction values*/
#define ILLEGAL_DIRECTION 0
#define SOUTH_DIRECTION 1
#define NORTH_DIRECTION 2
#define MAX_VALUE_DIRECTION 3

static const value_string tiedirectionvals[] = {
    { ILLEGAL_DIRECTION,      "Illegal" },
    { SOUTH_DIRECTION,        "South" },
    { NORTH_DIRECTION,        "North" },
    { MAX_VALUE_DIRECTION,    "MaxValue" },
    { 0, NULL }
};

/* TIE Types values */

#define ILLEGAL_TIE_TYPE  0
#define TIE_TYPE_MIN_VALUE 1
#define NODE_TIE_TYPE 2
#define PREFIX_TIE_TYPE 3
#define POSITIVE_DIS_PREFIX_TIE_TYPE 4
#define NEGATIVE_DIS_PREFIX_TIE_TYPE 5
#define PGP_PREFIX_TIE_TYPE 6
#define KEY_VALUE_TIE_TYPE 7
#define EXTERNAL_PREFIX_TIE_TYPE 8
#define POSITIVE_EXTERNAL_DIS_PREFIX_TIE_TYPE 9
#define TIE_TYPE_MAX_VALUE 10

static const value_string tieelemtypevals[] = {
    { ILLEGAL_TIE_TYPE,                      "Illegal"},
    { TIE_TYPE_MIN_VALUE,                    "MinValue"},
    { NODE_TIE_TYPE,                         "Node"},
    { PREFIX_TIE_TYPE,                       "Prefix"},
    { POSITIVE_DIS_PREFIX_TIE_TYPE,          "Positive Disaggregation Prefix"},
    { NEGATIVE_DIS_PREFIX_TIE_TYPE,          "Negative Disaggregation Prefix"},
    { PGP_PREFIX_TIE_TYPE,                   "PGP Prefix"},
    { KEY_VALUE_TIE_TYPE,                    "Key Value"},
    { EXTERNAL_PREFIX_TIE_TYPE,              "External Prefix"},
    { POSITIVE_EXTERNAL_DIS_PREFIX_TIE_TYPE, "Positive External Disaggregation Prefix"},
    { TIE_TYPE_MAX_VALUE,                    "MaxValue"},
    { 0, NULL}        
};

/* Hierarchy indications values */

#define LEAF_ONLY 0
#define LEAF_ONLY_AND_LEAF_2_LEAF_PROCEDURES 1
#define TOP_OF_FABRIC 2

static const value_string hierarchyindicationsvals[] = {
    { LEAF_ONLY,                               "Leaf only"},
    { LEAF_ONLY_AND_LEAF_2_LEAF_PROCEDURES,    "Leaf only and leaf-to-leaf procedures"},
    { TOP_OF_FABRIC,                           "Top-of-Fabric"},
    { 0, NULL}
};

/* Utils */ 

static int 
dissect_IEEE802_1ASTimeStampType(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, int offset) {

    guint field_type, field_id;

    /* Optional fields */
    field_type = tvb_get_guint8(tvb, offset);

    while (field_type != THRIFT_STRUCT_END) {

        field_id = tvb_get_guint16(tvb, offset + 1, ENC_BIG_ENDIAN);
        offset += THRIFT_FIELD_HEADER_LENGTH;

        switch (field_id){
            case 1: /* AS sec i64 */
                if (field_type != THRIFT_I64){
                    proto_tree_add_expert(tree, pinfo, &ei_rift_thrift_type_err, tvb, offset - THRIFT_FIELD_HEADER_LENGTH, 1);
                    return offset;
                }
                proto_tree_add_item(tree, hf_rift_ieee802_timestamp_sec, tvb, offset, 8, ENC_BIG_ENDIAN);
                offset += 8;
                break;
            case 2: /* AS nsec i32 */
                if (field_type != THRIFT_I32){
                    proto_tree_add_expert(tree, pinfo, &ei_rift_thrift_type_err, tvb, offset - THRIFT_FIELD_HEADER_LENGTH, 1);
                    return offset;
                }
                proto_tree_add_item(tree, hf_rift_ieee802_timestamp_nsec, tvb, offset, 4, ENC_BIG_ENDIAN);
                offset += 4;
                break;
            default:
                proto_tree_add_expert(tree, pinfo, &ei_rift_thrift_unexpected_field_id, tvb, offset + 1 - THRIFT_FIELD_HEADER_LENGTH, 2);
                return offset;
        }
        field_type = tvb_get_guint8(tvb, offset);
    }

    /* Reading struct end */
    offset += 1;

    return offset;
}

static int 
dissect_node_capabilities(tvbuff_t *tvb, packet_info *pinfo, proto_tree *parent_tree, int offset)
{
    proto_tree *tree;
    proto_item *ti;
    guint field_type, field_id;

    tree = proto_tree_add_subtree(parent_tree, tvb, offset, 0, ett_rift_node_cap, &ti,
                                  "Node capabilities");
   
    field_type = tvb_get_guint8(tvb, offset);

    while (field_type != THRIFT_STRUCT_END){
        field_id = tvb_get_guint16(tvb, offset + 1, ENC_BIG_ENDIAN);
        offset += THRIFT_FIELD_HEADER_LENGTH;

        switch (field_id){
            case 1: /* Minor Version */
                if (field_type != THRIFT_I16){
                    proto_tree_add_expert(tree, pinfo, &ei_rift_thrift_type_err, tvb, offset - THRIFT_FIELD_HEADER_LENGTH, 1);
                    return offset;
                }
                
                proto_tree_add_item(tree, hf_rift_node_capabilities_minor_version, tvb, offset, 2, ENC_BIG_ENDIAN);
                offset += 2;
                break;
            case 2: /* Flood Reduction */
                if (field_type != THRIFT_BOOL){
                    proto_tree_add_expert(tree, pinfo, &ei_rift_thrift_type_err, tvb, offset - THRIFT_FIELD_HEADER_LENGTH, 1);
                    return offset;
                }
                
                proto_tree_add_item(tree, hf_rift_node_capabilities_flood_reduction, tvb, offset, 1, ENC_BIG_ENDIAN);
                offset += 1;
                break;
            case 3: /* Hierarchy Indications */
                if (field_type != THRIFT_I32){
                    proto_tree_add_expert(tree, pinfo, &ei_rift_thrift_type_err, tvb, offset - THRIFT_FIELD_HEADER_LENGTH, 1);
                    return offset;
                }
                
                proto_tree_add_item(tree, hf_rift_node_capabilities_hierarchy_indications, tvb, offset, 4, ENC_BIG_ENDIAN);
                offset += 4;
                break;
            default:
                proto_tree_add_expert(tree, pinfo, &ei_rift_thrift_unexpected_field_id, tvb, offset + 1 - THRIFT_FIELD_HEADER_LENGTH, 2);
                return offset;
        }
        field_type = tvb_get_guint8(tvb, offset);
    }

    /* Reading struct end */
    offset += 1;

    proto_item_set_end(ti, tvb, offset);

    return offset;
}

static int 
dissect_node_flags(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, int offset)
{    
    guint field_type, field_id;

    field_type = tvb_get_guint8(tvb, offset);

    while (field_type != THRIFT_STRUCT_END){
        
        field_id = tvb_get_guint16(tvb, offset + 1, ENC_BIG_ENDIAN);
        offset += THRIFT_FIELD_HEADER_LENGTH;

        switch (field_id){
        case 1: /* Value of overload */
            if (field_type != THRIFT_BOOL){
                proto_tree_add_expert(tree, pinfo, &ei_rift_thrift_type_err, tvb, offset - THRIFT_FIELD_HEADER_LENGTH, 1);
                return offset;
            }               
            proto_tree_add_item(tree, hf_rift_node_flag_overload, tvb, offset, 1, ENC_BIG_ENDIAN);
            offset += 1;
            break;
        default:
            proto_tree_add_expert(tree, pinfo, &ei_rift_thrift_unexpected_field_id, tvb, offset + 1 - THRIFT_FIELD_HEADER_LENGTH, 2);
            return offset;

        field_type = tvb_get_guint8(tvb, offset);
        }
    }
    
    /* Reading struct end */
    offset += 1;
    
    return offset;
}


/* Start Packet Dissection */

/* Dissect TIE Origin Security Envelope Header of a TIE message */
static int
dissect_tie_sec_env_header(tvbuff_t *tvb, proto_tree *parent_tree, int offset)
{
    proto_tree *rift_tie_sec_header_tree;
    guint tie_fingerprint_len;

    /* Reading TIE TTL */
    proto_tree_add_item(parent_tree, hf_rift_tie_ttl, tvb, offset, 4, ENC_BIG_ENDIAN);
    offset += 4;

    /* Getting length of TIE fingerprint*/
    tie_fingerprint_len = tvb_get_guint8(tvb, offset + 3);
    tie_fingerprint_len *= 4;
        
    /* Creating subtree for TIE Origin Security Envelope Header */      
    rift_tie_sec_header_tree = proto_tree_add_subtree(parent_tree, tvb, offset, tie_fingerprint_len + 4, ett_rift_tie_sec_header, NULL,
                                                      "TIE Origin Security Envelope Header");
        
    /* Reading TIE Origin Key ID*/
    proto_tree_add_item(rift_tie_sec_header_tree, hf_rift_tie_origin_key_id, tvb, offset, 3, ENC_BIG_ENDIAN);
    offset += 3;

    /* Reading length of TIE fingerprint*/
    proto_tree_add_item(rift_tie_sec_header_tree, hf_rift_tie_fingerprint_len, tvb, offset, 1, ENC_BIG_ENDIAN);
    offset += 1;

    if (tie_fingerprint_len > 0) {
        /* Reading TIE Security Fingerprint of length 4 (32 bits) * tie_fingerprint_len */
        proto_tree_add_item(rift_tie_sec_header_tree, hf_rift_tie_fingerprint, tvb, offset, tie_fingerprint_len, ENC_BIG_ENDIAN);
        offset += tie_fingerprint_len;
    }
    return offset;
}

/* Dissect Outer Security Envelope Header of a message*/
static int
dissect_outer_sec_env_header(tvbuff_t *tvb, proto_tree *parent_tree, int offset)
{
    proto_item *ti;
    guint fingerprint_len;
    proto_tree *tree;

    /* Creating subtree for Outer Security Envelope Header*/      
    tree = proto_tree_add_subtree(parent_tree, tvb, offset, 0, ett_rift_sec_header, &ti,
                                                      "Outer Security Envelope Header");

    /* Reading RIFT Magic */
    proto_tree_add_item(tree, hf_rift_magic, tvb,
                        offset, 2, ENC_BIG_ENDIAN);
    offset += 2;
    
    /* Reading packet number */
    proto_tree_add_item(tree, hf_rift_pkt_number, tvb,
                        offset, 2, ENC_BIG_ENDIAN);
    offset += 2;

    /* Reading reserved byte */
    proto_tree_add_item(tree, hf_rift_reserved, tvb,
                        offset, 1, ENC_NA);
    offset += 1;

    /* Reading major version */
    proto_tree_add_item(tree, hf_rift_major_version, tvb,
                        offset, 1, ENC_BIG_ENDIAN);
    offset += 1;

    /* Reading Outer Key ID */
    proto_tree_add_item(tree, hf_rift_outer_key_id, tvb, offset, 1, ENC_BIG_ENDIAN);

    offset += 1;

    /* Getting length of fingerprint */
    fingerprint_len = tvb_get_guint8(tvb, offset);
    fingerprint_len *= 4;
        
    /* Reading Fingerprint length */
    proto_tree_add_item(tree, hf_rift_fingerprint_len, tvb, offset, 1, ENC_BIG_ENDIAN);
    offset += 1;

    if (fingerprint_len > 0) {
        /* Reading Security Fingerprint of length 4 (32 bits) * fingerprint_len */
        proto_tree_add_item(tree, hf_rift_fingerprint, tvb, offset, fingerprint_len, ENC_BIG_ENDIAN);
        offset += fingerprint_len;
    }

    /* Reading Weak Local Nonce */ 
    proto_tree_add_item(tree, hf_rift_weak_nonce_local, tvb, offset, 2, ENC_BIG_ENDIAN);
    offset += 2;

    /* Reading Weak Remote Nonce */
    proto_tree_add_item(tree, hf_rift_weak_nonce_remote, tvb, offset, 2, ENC_BIG_ENDIAN);
    offset += 2;

    if (tvb_get_guint32(tvb, offset, ENC_BIG_ENDIAN) != NO_TIE_TTL) {
        offset = dissect_tie_sec_env_header(tvb, tree, offset);
    } else
        offset += 4;

    proto_item_set_end(ti, tvb, offset);

    return offset;
}

/* The way of decoding bytes inside Serialized RIFT Model Object is based in Thrift Binary Protocol
 * explicitly described in official Thrift docs:
 * https://github.com/apache/thrift/blob/master/doc/specs/thrift-binary-protocol.md
 */

/* NOTE: Actions for unexpected values should be added later */
static int
dissect_packet_header(tvbuff_t *tvb, packet_info *pinfo, proto_tree *parent_tree, int offset)
{
    guint field_type, field_id;
    
    field_type = tvb_get_guint8(tvb, offset);

    while (field_type != THRIFT_STRUCT_END){

        field_id = tvb_get_guint16(tvb, offset + 1, ENC_BIG_ENDIAN);
        offset += THRIFT_FIELD_HEADER_LENGTH;
        
        switch (field_id){
        case 1: /* Major version */
            if (field_type != THRIFT_BYTE){
                proto_tree_add_expert(parent_tree, pinfo, &ei_rift_thrift_type_err, tvb, offset - THRIFT_FIELD_HEADER_LENGTH, 1);
                return offset;
            }
            /* Adding major version field to the tree*/
            proto_tree_add_item(parent_tree, hf_rift_ser_major_version, tvb, offset, 1, ENC_BIG_ENDIAN);
            offset += 1;
            break;
        case 2: /* Minor version */
            if (field_type != THRIFT_I16){
                proto_tree_add_expert(parent_tree, pinfo, &ei_rift_thrift_type_err, tvb, offset - THRIFT_FIELD_HEADER_LENGTH, 1);
                return offset;
            }
            /* Adding minor version field to the tree */
            proto_tree_add_item(parent_tree, hf_rift_ser_minor_version, tvb, offset, 2, ENC_BIG_ENDIAN);
            offset += 2;
            break;
        case 3: /* Sender */
            if (field_type != THRIFT_I64){
                proto_tree_add_expert(parent_tree, pinfo, &ei_rift_thrift_type_err, tvb, offset - THRIFT_FIELD_HEADER_LENGTH, 1);
                return offset;
            }
            /* Adding sender field to the tree */
            proto_tree_add_item(parent_tree, hf_rift_ser_sender, tvb, offset, 8, ENC_BIG_ENDIAN);
            offset += 8;
            break;
        case 4: /* Level */
            if (field_type != THRIFT_BYTE){
                proto_tree_add_expert(parent_tree, pinfo, &ei_rift_thrift_type_err, tvb, offset - THRIFT_FIELD_HEADER_LENGTH, 1);
                return offset;
            }
            /* Adding level field to the tree */
            proto_tree_add_item(parent_tree, hf_rift_ser_lvl, tvb, offset, 1, ENC_BIG_ENDIAN);
            offset += 1;
            break;
        default: /*Unexpected value*/
            proto_tree_add_expert(parent_tree, pinfo, &ei_rift_thrift_unexpected_field_id, tvb, offset + 1 - THRIFT_FIELD_HEADER_LENGTH, 2);
            return offset;
        }
        
        field_type = tvb_get_guint8(tvb, offset);
    }

    /* Reading struct end*/
    offset += 1;

    return offset;
}

static int
dissect_tieid(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, int offset)
{
    guint field_type, field_id;
    guint32 dir, type, num;
    guint64 orig;
    const gchar *dir_str, *type_str;

    field_type = tvb_get_guint8(tvb, offset);

    while (field_type != THRIFT_STRUCT_END) {
        
        field_id = tvb_get_guint16(tvb, offset + 1, ENC_BIG_ENDIAN);
        offset += THRIFT_FIELD_HEADER_LENGTH;

        switch (field_id) {
            case 1: /* Direction */
                if (field_type != THRIFT_I32){
                    proto_tree_add_expert(tree, pinfo, &ei_rift_thrift_type_err, tvb, offset - THRIFT_FIELD_HEADER_LENGTH, 1);
                    return offset;
                }
                /* Adding TIE Direction to the tree */
                proto_tree_add_item(tree, hf_rift_tie_direction, tvb, offset, 4, ENC_BIG_ENDIAN);
                offset += 4;

                dir = tvb_get_guint32(tvb, offset - 4, ENC_BIG_ENDIAN);
                if ((dir_str = try_val_to_str(dir, tiedirectionvals)))
                    col_append_fstr(pinfo->cinfo, COL_INFO, " Direction: %s ", dir_str);
                else /* Handle unexpected value */
                    proto_tree_add_expert(tree, pinfo, &ei_rift_tie_dir_err, tvb, offset - 4, 4);
                
                break;
            case 2: /* Originator */
                if (field_type != THRIFT_I64){
                    proto_tree_add_expert(tree, pinfo, &ei_rift_thrift_type_err, tvb, offset - THRIFT_FIELD_HEADER_LENGTH, 1);
                    return offset;
                }
                /* Adding TIE Originator to the tree */
                proto_tree_add_item(tree, hf_rift_tie_originator, tvb, offset, 8, ENC_BIG_ENDIAN);
                offset += 8;

                orig = tvb_get_guint64(tvb, offset - 8, ENC_BIG_ENDIAN);
                col_append_fstr(pinfo->cinfo, COL_INFO, " From: %"PRIu64" ", orig);
                break;
            case 3: /* Type of TIE Packet */
                if (field_type != THRIFT_I32){
                    proto_tree_add_expert(tree, pinfo, &ei_rift_thrift_type_err, tvb, offset - THRIFT_FIELD_HEADER_LENGTH, 1);
                    return offset;
                }
                /* Adding TIE Element Type to the tree */
                proto_tree_add_item(tree, hf_rift_tie_elem_type, tvb, offset, 4, ENC_BIG_ENDIAN);
                offset += 4;

                type = tvb_get_guint32(tvb, offset - 4, ENC_BIG_ENDIAN);
                if ((type_str = try_val_to_str(type, tieelemtypevals)))
                    col_append_fstr(pinfo->cinfo, COL_INFO, " Type: %s ", type_str);
                else /* Handle unexpected value */
                    proto_tree_add_expert(tree, pinfo, &ei_rift_tie_elem_err, tvb, offset - 4, 4);
                
                break;
            case 4: /* Number of TIE */
                if (field_type != THRIFT_I32){
                    proto_tree_add_expert(tree, pinfo, &ei_rift_thrift_type_err, tvb, offset - THRIFT_FIELD_HEADER_LENGTH, 1);
                    return offset;
                }
                /* Adding TIE Number to the tree */
                proto_tree_add_item(tree, hf_rift_tie_number, tvb, offset, 4, ENC_BIG_ENDIAN);
                offset += 4;

                num = tvb_get_guint32(tvb, offset - 4, ENC_BIG_ENDIAN);
                col_append_fstr(pinfo->cinfo, COL_INFO, " Number: %u ", num);
                break;
            default: /* Unexpected value */
                proto_tree_add_expert(tree, pinfo, &ei_rift_thrift_unexpected_field_id, tvb, offset + 1 - THRIFT_FIELD_HEADER_LENGTH, 2);
                return offset;
        }
        
        field_type = tvb_get_guint8(tvb, offset);
    }

    /* Reading struct end */
    offset += 1;

    return offset;
}

static int 
dissect_tie_header(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, int offset)
{         
    guint field_type, field_id;

    field_type = tvb_get_guint8(tvb, offset);

    while (field_type != THRIFT_STRUCT_END){

        field_id = tvb_get_guint16(tvb, offset + 1, ENC_BIG_ENDIAN);
        offset += THRIFT_FIELD_HEADER_LENGTH;

        switch (field_id){
            case 2: /* TIE ID */
                if (field_type != THRIFT_STRUCT){
                    proto_tree_add_expert(tree, pinfo, &ei_rift_thrift_type_err, tvb, offset - THRIFT_FIELD_HEADER_LENGTH, 1);
                    return offset;
                }
                offset = dissect_tieid(tvb, pinfo, tree, offset);
                break;
            case 3:
                if (field_type != THRIFT_I64){
                    proto_tree_add_expert(tree, pinfo, &ei_rift_thrift_type_err, tvb, offset - THRIFT_FIELD_HEADER_LENGTH, 1);
                    return offset;
                }
                /* Adding sequence number to the tree */
                proto_tree_add_item(tree, hf_rift_tie_id_seqnum, tvb, offset, 8, ENC_BIG_ENDIAN);
                offset += 8;   
                break;
            case 10: /* origination_time */
                if (field_type != THRIFT_STRUCT){
                    proto_tree_add_expert(tree, pinfo, &ei_rift_thrift_type_err, tvb, offset - THRIFT_FIELD_HEADER_LENGTH, 1);
                    return offset;
                }
                offset = dissect_IEEE802_1ASTimeStampType(tvb, pinfo, tree, offset);
                break;
            case 12: /* origination_lifetime */
                if (field_type != THRIFT_I32){
                    proto_tree_add_expert(tree, pinfo, &ei_rift_thrift_type_err, tvb, offset - THRIFT_FIELD_HEADER_LENGTH, 1);
                    return offset;
                }
                /* Adding origination lifetime to the tree */
                proto_tree_add_item(tree, hf_rift_tie_lifetime, tvb, offset, 4, ENC_BIG_ENDIAN);
                offset += 4;
                break; 
            default:
                proto_tree_add_expert(tree, pinfo, &ei_rift_thrift_unexpected_field_id, tvb, offset + 1 - THRIFT_FIELD_HEADER_LENGTH, 2);
                return offset;
        }
        
        field_type = tvb_get_guint8(tvb, offset);
    }

    /* Reading struct end*/
    offset += 1;

    return offset;
}

/* TIE Element Dissection */

static int
dissect_link_id_pairs(tvbuff_t *tvb, packet_info *pinfo, proto_tree *parent_tree, int offset)
{
    proto_tree *link;
    proto_tree *li;
    guint field_type, field_id,
        local_id, remote_id, pf_name_len;
    
    field_type = tvb_get_guint8(tvb, offset);

    link = proto_tree_add_subtree(parent_tree, tvb, offset, 0, ett_rift_tie_link_id_pair, &li,
                                  "(");

    while (field_type != THRIFT_STRUCT_END){
        
        field_id = tvb_get_guint16(tvb, offset + 1, ENC_BIG_ENDIAN);
        offset += THRIFT_FIELD_HEADER_LENGTH;
        
        switch (field_id){
        case 1: /* Local id */
            if (field_type != THRIFT_I32){
                proto_tree_add_expert(link, pinfo, &ei_rift_thrift_type_err, tvb, offset - THRIFT_FIELD_HEADER_LENGTH, 1);
                return offset;
            }
            local_id = tvb_get_guint32(tvb, offset, ENC_BIG_ENDIAN);
            proto_item_append_text(link, "%u, ", local_id);
            proto_tree_add_item(link, hf_rift_local_link_id_type, tvb, offset, 4, ENC_BIG_ENDIAN);
            offset += 4;           
            break;
        case 2: /* Remote id */
            if (field_type != THRIFT_I32){
                proto_tree_add_expert(link, pinfo, &ei_rift_thrift_type_err, tvb, offset - THRIFT_FIELD_HEADER_LENGTH, 1);
                return offset;
            }
            remote_id = tvb_get_guint32(tvb, offset, ENC_BIG_ENDIAN);
            proto_item_append_text(link, "%u", remote_id);
            proto_tree_add_item(link, hf_rift_remote_link_id_type, tvb, offset, 4, ENC_BIG_ENDIAN);
            offset += 4;
            break;
        case 10: /* Platform interface index */
            if (field_type != THRIFT_I32){
                proto_tree_add_expert(link, pinfo, &ei_rift_thrift_type_err, tvb, offset - THRIFT_FIELD_HEADER_LENGTH, 1);
                return offset;
            }
            proto_tree_add_item(link, hf_rift_platform_if_index, tvb, offset, 4, ENC_BIG_ENDIAN);
            offset += 4;
            break;
        case 11: /* Platform interface name */
            if (field_type != THRIFT_STRING){
                proto_tree_add_expert(link, pinfo, &ei_rift_thrift_type_err, tvb, offset - THRIFT_FIELD_HEADER_LENGTH, 1);
                return offset;
            }
            pf_name_len = tvb_get_guint32(tvb, offset, ENC_BIG_ENDIAN);
            proto_tree_add_item(link, hf_rift_platform_if_name, tvb, offset, pf_name_len, ENC_BIG_ENDIAN);
            offset += (4 + pf_name_len);        
            break;
        case 12: /* Trusted outer security key */
            if (field_type != THRIFT_BYTE){
                proto_tree_add_expert(link, pinfo, &ei_rift_thrift_type_err, tvb, offset - THRIFT_FIELD_HEADER_LENGTH, 1);
                return offset;
            }
            proto_tree_add_item(link, hf_rift_link_id_sec_key, tvb, offset, 1, ENC_BIG_ENDIAN);
            offset += 1;
            break;
        case 13: /* BFD up */
            if (field_type != THRIFT_BOOL){
                proto_tree_add_expert(link, pinfo, &ei_rift_thrift_type_err, tvb, offset - THRIFT_FIELD_HEADER_LENGTH, 1);
                return offset;
            }
            proto_tree_add_item(link, hf_rift_bfd_up, tvb, offset, 1, ENC_BIG_ENDIAN);
            offset += 1;
            break;
        default:
            proto_tree_add_expert(link, pinfo, &ei_rift_thrift_unexpected_field_id, tvb, offset + 1 - THRIFT_FIELD_HEADER_LENGTH, 2);
            return offset;
        }

        field_type = tvb_get_guint8(tvb, offset);
    }

    proto_item_append_text(link, ")");
    
    /* Reading struct end*/
    offset += 1;

    proto_item_set_end(li, tvb, offset);

    return offset;
}

static int
dissect_node_neighbors_tie_elem(tvbuff_t *tvb, packet_info *pinfo, proto_tree *parent_tree, int offset)
{
    proto_tree *links;
    proto_item *li;
    guint field_type, field_id,
        set_size;
    
    field_type = tvb_get_guint8(tvb, offset);

    while (field_type != THRIFT_STRUCT_END){
        
        field_id = tvb_get_guint16(tvb, offset + 1, ENC_BIG_ENDIAN);
        offset += THRIFT_FIELD_HEADER_LENGTH;

        switch (field_id){
        case 1: /* Level */
            if (field_type != THRIFT_BYTE){
                proto_tree_add_expert(parent_tree, pinfo, &ei_rift_thrift_type_err, tvb, offset - THRIFT_FIELD_HEADER_LENGTH, 1);
                return offset;
            }
            /* Adding level to the tree */
            proto_tree_add_item(parent_tree, hf_rift_node_neighbor_level, tvb, offset, 1, ENC_BIG_ENDIAN);
            offset += 1;
            break;
        case 3: /* Cost */
            if (field_type != THRIFT_I32){
                proto_tree_add_expert(parent_tree, pinfo, &ei_rift_thrift_type_err, tvb, offset - THRIFT_FIELD_HEADER_LENGTH, 1);
                return offset;
            }
            /* Adding cost to the tree */
            proto_tree_add_item(parent_tree, hf_rift_node_neighbor_cost, tvb, offset, 4, ENC_BIG_ENDIAN);
            offset += 4;
            break;
        case 4: /* Link IDs */
            if (field_type != THRIFT_SET){
                proto_tree_add_expert(parent_tree, pinfo, &ei_rift_thrift_type_err, tvb, offset - THRIFT_FIELD_HEADER_LENGTH, 1);
                return offset;
            }
            
            /* Getting set type and size*/
            field_type = tvb_get_guint8(tvb, offset);
            set_size = tvb_get_guint32(tvb, offset + 1, ENC_BIG_ENDIAN);
            offset += THRIFT_SET_HEADER_LENGTH;

            links = proto_tree_add_subtree(parent_tree, tvb, offset, 0, ett_rift_tie_link_id_pairs, &li,
                                           "Total links:");                    
            proto_item_append_text(li, " %u", set_size);

                    
            /* Dissect every element of set */
            while (set_size > 0){
                offset = dissect_link_id_pairs(tvb, pinfo, links, offset);
                set_size -= 1;
            }

            /* Marking end of set */
            proto_item_set_end(li, tvb, offset);
            break;
        case 5: /* Bandwidth */
            if (field_type != THRIFT_I32){
                proto_tree_add_expert(parent_tree, pinfo, &ei_rift_thrift_type_err, tvb, offset - THRIFT_FIELD_HEADER_LENGTH, 1);
                return offset;
            }
            /* Adding bandwidth to the tree */
            proto_tree_add_item(parent_tree, hf_rift_node_neighbor_bandwidth, tvb, offset, 4, ENC_BIG_ENDIAN);
            offset += 4;
            break;
        default:
            proto_tree_add_expert(parent_tree, pinfo, &ei_rift_thrift_unexpected_field_id, tvb, offset + 1 - THRIFT_FIELD_HEADER_LENGTH, 2);
            return offset;
        }

        field_type = tvb_get_guint8(tvb, offset);
    }

    /* Reading struct end */
    offset += 1;
    
    return offset;
}

static int
dissect_node_neighbors(tvbuff_t *tvb, packet_info *pinfo, proto_tree *parent_tree, int offset)
{
    proto_tree *neighbors, *neighbor;
    proto_item *nsi, *ni;
    guint key_type, value_type,
        map_size;
    guint64 key;

    /* Getting header of Thrift map */
    key_type = tvb_get_guint8(tvb, offset);
    value_type = tvb_get_guint8(tvb, offset + 1);
    map_size = tvb_get_guint32(tvb, offset + 2, ENC_BIG_ENDIAN);

    offset += THRIFT_MAP_HEADER_LENGTH;
    
    if (key_type != THRIFT_I64){
        proto_tree_add_expert(parent_tree, pinfo, &ei_rift_thrift_type_err, tvb, offset - THRIFT_MAP_HEADER_LENGTH, 1);
        return offset;
    }
    
    if (value_type != THRIFT_STRUCT){
        proto_tree_add_expert(parent_tree, pinfo, &ei_rift_thrift_type_err, tvb, offset + 1 - THRIFT_FIELD_HEADER_LENGTH, 1);
        return offset;
    }

    /* Creating subtree for neighbors */
    neighbors = proto_tree_add_subtree(parent_tree, tvb, offset, 0, ett_rift_tie_neighbors, &nsi,
                                  "Total neighbors:");

    proto_item_append_text(nsi, " %u ", map_size);

    /* Iterating through neighbors map */
    while (map_size > 0)
        {
            
            /* Getting key value */
            key = tvb_get_guint64(tvb, offset, ENC_BIG_ENDIAN);
            
            neighbor = proto_tree_add_subtree(neighbors, tvb, offset, 0, ett_rift_tie_neighbor, &ni,
                                              "Neighbor");

            proto_item_append_text(ni, " %"PRIu64, key);
            proto_tree_add_item(neighbor, hf_rift_node_neighbor_id, tvb, offset, 8, ENC_BIG_ENDIAN);
            
            offset += 8;

            /* Dissect Node Neighbors TIE Element*/
            offset = dissect_node_neighbors_tie_elem(tvb, pinfo, neighbor, offset);
            
            proto_item_set_end(ni, tvb, offset);

            map_size -= 1;
        }

    /* Setting end of neighbors list in buffer*/
    proto_item_set_end(nsi, tvb, offset);
    
    return offset;
}

static int
dissect_miscabled_link_id_type(tvbuff_t *tvb, packet_info *pinfo, proto_tree *parent_tree, int offset)
{
    proto_item *li;
    proto_tree *links;
    guint field_type, set_size;

    /* Getting set type and size*/
    field_type = tvb_get_guint8(tvb, offset);

    /* Type should be i32 */
    if (field_type != THRIFT_I32){
        proto_tree_add_expert(parent_tree, pinfo, &ei_rift_thrift_type_err, tvb, offset, 1);
        return offset;
    }
    
    set_size = tvb_get_guint32(tvb, offset + 1, ENC_BIG_ENDIAN);
    offset += THRIFT_SET_HEADER_LENGTH;

    /* Adding subtree for links */
    links = proto_tree_add_subtree(parent_tree, tvb, offset, 0, ett_rift_tie_miscabled_link_id, &li,
                                   "Total miscabled links:");

    proto_item_append_text(li, " %u ", set_size);

    /* Dissect every element of set */
    while (set_size > 0){
        proto_tree_add_item(links, hf_rift_miscabled_link_id_type, tvb, offset, 4, ENC_BIG_ENDIAN);
        offset += 4;
        set_size -= 1;
    }

    /* Marking end of set */
    proto_item_set_end(li, tvb, offset);

    return offset;
}


static int
dissect_tie_node_elem(tvbuff_t *tvb, packet_info *pinfo, proto_tree *parent_tree, int offset)
{

    proto_item *ti;
    proto_tree *tree;
    guint field_type, field_id,
        node_name_len;


    tree = proto_tree_add_subtree(parent_tree, tvb, offset, 0, ett_rift_tie_node, &ti,
                                                      "Node TIE");
    
    field_type = tvb_get_guint8(tvb, offset);

    while (field_type != THRIFT_STRUCT_END){
            
        field_id = tvb_get_guint16(tvb, offset + 1, ENC_BIG_ENDIAN);
        offset += THRIFT_FIELD_HEADER_LENGTH;

        switch (field_id){
            case 1: /* Node Level (i8) */
                if (field_type != THRIFT_BYTE){
                    proto_tree_add_expert(tree, pinfo, &ei_rift_thrift_type_err, tvb, offset - THRIFT_FIELD_HEADER_LENGTH, 1);
                    return offset;
                }
                /* Adding Node level to the tree */
                proto_tree_add_item(tree, hf_rift_tie_node_level, tvb, offset, 1, ENC_BIG_ENDIAN);
                offset += 1;
                break;
            case 2: /* Nodes Neighbors Map */
                if (field_type != THRIFT_MAP){
                    proto_tree_add_expert(tree, pinfo, &ei_rift_thrift_type_err, tvb, offset - THRIFT_FIELD_HEADER_LENGTH, 1);
                    return offset;
                }
                offset = dissect_node_neighbors(tvb, pinfo, tree, offset);
                break;
            case 3: /* Node Capabilities */
                if (field_type != THRIFT_STRUCT){
                    proto_tree_add_expert(tree, pinfo, &ei_rift_thrift_type_err, tvb, offset - THRIFT_FIELD_HEADER_LENGTH, 1);
                    return offset;
                }
                offset = dissect_node_capabilities(tvb, pinfo, tree, offset);
                break;
            case 4: /* Node Flags (bool) */
                if (field_type != THRIFT_STRUCT){
                    proto_tree_add_expert(tree, pinfo, &ei_rift_thrift_type_err, tvb, offset - THRIFT_FIELD_HEADER_LENGTH, 1);
                    return offset;
                }
                offset = dissect_node_flags(tvb, pinfo, tree, offset);
                break;
            case 5: /* Node Name (string) */
                if (field_type != THRIFT_STRING){
                    proto_tree_add_expert(tree, pinfo, &ei_rift_thrift_type_err, tvb, offset - THRIFT_FIELD_HEADER_LENGTH, 1);
                    return offset;
                }
                /* Reading string length */
                node_name_len = tvb_get_guint32(tvb, offset, ENC_BIG_ENDIAN);
                    
                /* Adding Node name to the tree */
                proto_tree_add_item(tree, hf_rift_tie_node_name, tvb, offset + 4, node_name_len, ENC_BIG_ENDIAN);
                offset += (4 + node_name_len);
                break; 
            case 6:  /* PoD (i32) */
                if (field_type != THRIFT_I32){
                    proto_tree_add_expert(tree, pinfo, &ei_rift_thrift_type_err, tvb, offset - THRIFT_FIELD_HEADER_LENGTH, 1);
                    return offset;
                }
                /* Adding PoD to the tree*/
                proto_tree_add_item(tree, hf_rift_tie_node_pod, tvb, offset, 4, ENC_BIG_ENDIAN);
                offset += 4;
                break;
            case 10: /* Miscabled links Map */
                if (field_type != THRIFT_SET){
                    proto_tree_add_expert(tree, pinfo, &ei_rift_thrift_type_err, tvb, offset - THRIFT_FIELD_HEADER_LENGTH, 1);
                    return offset;
                }
                offset = dissect_miscabled_link_id_type(tvb, pinfo, tree, offset);
                break;
            default:
                proto_tree_add_expert(tree, pinfo, &ei_rift_thrift_unexpected_field_id, tvb, offset + 1 - THRIFT_FIELD_HEADER_LENGTH, 2);
                return offset;
        }

        field_type = tvb_get_guint8(tvb, offset);
    
    }

    /* Reading end of struct */
    offset += 1;

    proto_item_set_end(ti, tvb, offset);
    
    return offset;
}

static int
dissect_tags(tvbuff_t *tvb, packet_info *pinfo, proto_tree *parent_tree, int offset)
{

    proto_item *ti;
    proto_tree *tags;
    guint field_type, set_size;

    /* Getting set type and size*/
    field_type = tvb_get_guint8(tvb, offset);

    /* Type should be i64 */
    if (field_type != THRIFT_I64){
        proto_tree_add_expert(parent_tree, pinfo, &ei_rift_thrift_type_err, tvb, offset, 1);
        return offset;
    }
    
    set_size = tvb_get_guint32(tvb, offset + 1, ENC_BIG_ENDIAN);
    offset += THRIFT_SET_HEADER_LENGTH;

    /* Adding subtree for links */
    tags = proto_tree_add_subtree(parent_tree, tvb, offset, 0, ett_rift_tie_prefix_attr_route_tags, &ti,
                                   "Total tags:");

    proto_item_append_text(ti, " %u ", set_size);

    /* Dissect every element of set */
    while (set_size > 0){
        proto_tree_add_item(tags, hf_rift_tie_prefix_attr_route_tag, tvb, offset, 8, ENC_BIG_ENDIAN);
        offset += 8;
        set_size -= 1;
    }

    /* Marking end of set */
    proto_item_set_end(ti, tvb, offset);    
    
    return offset;
}

static int
dissect_ip_prefix(tvbuff_t *tvb, packet_info *pinfo, proto_tree *parent_tree, proto_item *parent_item, int offset)
{
    guint field_type, field_id,
        pfx_len;
    guint32 addr_len;

    field_type = tvb_get_guint8(tvb, offset);
    field_id = tvb_get_guint16(tvb, offset + 1, ENC_BIG_ENDIAN);

    if (field_type != THRIFT_STRUCT){
        proto_tree_add_expert(parent_tree, pinfo, &ei_rift_thrift_type_err, tvb, offset, 1);
        return offset;
    }

    offset += THRIFT_FIELD_HEADER_LENGTH;
    
    switch (field_id){
    case 1: /* IPv4 prefix */
            
        field_type = tvb_get_guint8(tvb, offset);

        while (field_type != THRIFT_STRUCT_END){
            field_id = tvb_get_guint16(tvb, offset + 1, ENC_BIG_ENDIAN);
            offset += THRIFT_FIELD_HEADER_LENGTH;

            switch (field_id){
            case 1: /* IPv4 address */
                if (field_type != THRIFT_I32){
                    proto_tree_add_expert(parent_tree, pinfo, &ei_rift_thrift_type_err, tvb, offset - THRIFT_FIELD_HEADER_LENGTH, 1);
                    return offset;
                }
                /* Getting IPv4 address from buffer */
                proto_tree_add_item(parent_tree, hf_rift_tie_prefix_ipv4, tvb, offset, 4, ENC_BIG_ENDIAN);
                offset += 4;

                proto_item_prepend_text(parent_item, "%s", tvb_address_to_str(wmem_packet_scope(), tvb, AT_IPv4, offset - 4));
                break;
            case 2: /* Prefix length */
                if (field_type != THRIFT_BYTE){
                    proto_tree_add_expert(parent_tree, pinfo, &ei_rift_thrift_type_err, tvb, offset - THRIFT_FIELD_HEADER_LENGTH, 1);
                    return offset;
                }
                pfx_len = tvb_get_guint8(tvb, offset);
                proto_tree_add_item(parent_tree, hf_rift_tie_prefix_len, tvb, offset, 1, ENC_BIG_ENDIAN);                          
                offset += 1;

                proto_item_append_text(parent_item, "/%u", pfx_len);
                break;
            default:
                proto_tree_add_expert(parent_tree, pinfo, &ei_rift_thrift_unexpected_field_id, tvb, offset + 1 - THRIFT_FIELD_HEADER_LENGTH, 2);
                return offset;
            }                
                    
            field_type = tvb_get_guint8(tvb, offset);
        }

        /* Reading struct end */
        offset += 1;
        break;
    case 2: /* IPv6 prefix */
        field_type = tvb_get_guint8(tvb, offset);
                
        while (field_type != THRIFT_STRUCT_END){
            field_id = tvb_get_guint16(tvb, offset + 1, ENC_BIG_ENDIAN);
            offset += THRIFT_FIELD_HEADER_LENGTH;

            switch (field_id){
            case 1: /* IPv6 address */
                if (field_type != THRIFT_STRING){
                    proto_tree_add_expert(parent_tree, pinfo, &ei_rift_thrift_type_err, tvb, offset - THRIFT_FIELD_HEADER_LENGTH, 1);
                    return offset;
                }
                addr_len = tvb_get_guint32(tvb, offset, ENC_BIG_ENDIAN);

                if (addr_len > 0){
                    /* Getting IPv4 address from buffer */
                    proto_tree_add_item(parent_tree, hf_rift_tie_prefix_ipv6, tvb, offset + 4, addr_len, ENC_BIG_ENDIAN);
                    offset += (4 + addr_len);
                       
                    proto_item_prepend_text(parent_item, "%s", tvb_address_to_str(wmem_packet_scope(), tvb, AT_IPv6, offset - addr_len));
                } else {/* Handling all zeros IPv6 Address */
                    proto_item_prepend_text(parent_item, "::");
                    offset += 4;
                }
                break;
            case 2: /* Prefix length */
                if (field_type != THRIFT_BYTE){
                    proto_tree_add_expert(parent_tree, pinfo, &ei_rift_thrift_type_err, tvb, offset - THRIFT_FIELD_HEADER_LENGTH, 1);
                    return offset;
                }
                pfx_len = tvb_get_guint8(tvb, offset);
                proto_tree_add_item(parent_tree, hf_rift_tie_prefix_len, tvb, offset, 1, ENC_BIG_ENDIAN);
                offset += 1;
                       
                proto_item_append_text(parent_item, "/%u", pfx_len);
                break;
            default:
                proto_tree_add_expert(parent_tree, pinfo, &ei_rift_thrift_unexpected_field_id, tvb, offset + 1 - THRIFT_FIELD_HEADER_LENGTH, 2);
                return offset;
            }                
                    
            field_type = tvb_get_guint8(tvb, offset);
        }

        /* Reading struct end */
        offset += 1;
    }

    /* Reading struct end */
    offset += 1;

    return offset;
}

static int
dissect_prefix_attr(tvbuff_t *tvb, packet_info *pinfo, proto_tree *parent_tree, int offset)
{
    proto_item *ai, *mci;
    proto_tree *attr, *mon_clock;
    guint field_type, field_id;
    
    field_type = tvb_get_guint8(tvb, offset);

    attr = proto_tree_add_subtree(parent_tree, tvb, offset, 0, ett_rift_tie_prefix_attr, &ai,
                                      "Attributes");

    while (field_type != THRIFT_STRUCT_END){
        field_id = tvb_get_guint16(tvb, offset + 1, ENC_BIG_ENDIAN);
        offset += THRIFT_FIELD_HEADER_LENGTH;

        switch (field_id){
        case 2: /* Metric */
            if (field_type != THRIFT_I32){
                proto_tree_add_expert(attr, pinfo, &ei_rift_thrift_type_err, tvb, offset - THRIFT_FIELD_HEADER_LENGTH, 1);
                return offset;
            }
            proto_tree_add_item(attr, hf_rift_tie_prefix_attr_metric, tvb, offset, 4, ENC_BIG_ENDIAN);
            offset += 4;            
            break;
        case 3: /* Tags */
            if (field_type != THRIFT_SET){
                proto_tree_add_expert(attr, pinfo, &ei_rift_thrift_type_err, tvb, offset - THRIFT_FIELD_HEADER_LENGTH, 1);
                return offset;
            }
            offset = dissect_tags(tvb, pinfo, attr, offset);
            break;
        case 4: /* Monotonic clock */

            if (field_type != THRIFT_STRUCT){
                proto_tree_add_expert(attr, pinfo, &ei_rift_thrift_type_err, tvb, offset - THRIFT_FIELD_HEADER_LENGTH, 1);
                return offset;
            }
            
            mon_clock = proto_tree_add_subtree(attr, tvb, offset, 0, ett_rift_tie_prefix_attr_mon_clock, &mci, "Monotonic clock");
                
            field_type = tvb_get_guint8(tvb, offset);

            while (field_type != THRIFT_STRUCT_END){
                field_id = tvb_get_guint16(tvb, offset + 1, ENC_BIG_ENDIAN);
                offset += THRIFT_FIELD_HEADER_LENGTH;

                switch (field_id){
                case 1: /* Timestamp */
                    if (field_type != THRIFT_STRUCT){
                        proto_tree_add_expert(mon_clock, pinfo, &ei_rift_thrift_type_err, tvb, offset - THRIFT_FIELD_HEADER_LENGTH, 1);
                        return offset;
                    }
                    offset = dissect_IEEE802_1ASTimeStampType(tvb, pinfo, mon_clock, offset);
                    break;
                case 2: /* Prefix Transaction ID */
                    if (field_type != THRIFT_BYTE){
                        proto_tree_add_expert(mon_clock, pinfo, &ei_rift_thrift_type_err, tvb, offset - THRIFT_FIELD_HEADER_LENGTH, 1);
                        return offset;
                    }
                    proto_tree_add_item(mon_clock, hf_rift_tie_prefix_attr_trans_id, tvb, offset, 1, ENC_BIG_ENDIAN);
                    offset += 1;
                    break;
                default: /* Unexpected value */
                    proto_tree_add_expert(mon_clock, pinfo, &ei_rift_thrift_unexpected_field_id, tvb, offset + 1 - THRIFT_FIELD_HEADER_LENGTH, 2);
                    return offset;
                }

                field_type = tvb_get_guint8(tvb, offset);
            }
                
            /* Reading struct end */
            offset += 1;

            proto_item_set_end(mci, tvb, offset);
            break;
        case 6: /* Loopback */
            if (field_type != THRIFT_BOOL){
                proto_tree_add_expert(attr, pinfo, &ei_rift_thrift_type_err, tvb, offset - THRIFT_FIELD_HEADER_LENGTH, 1);
                return offset;
            }
            proto_tree_add_item(attr, hf_rift_tie_prefix_attr_loopback, tvb, offset, 1, ENC_BIG_ENDIAN);
            offset += 1;
            break;
        case 7: /* Directly attached */
            if (field_type != THRIFT_BOOL){
                proto_tree_add_expert(attr, pinfo, &ei_rift_thrift_type_err, tvb, offset - THRIFT_FIELD_HEADER_LENGTH, 1);
                return offset;
            }
            proto_tree_add_item(attr, hf_rift_tie_prefix_attr_dir_attached, tvb, offset, 1, ENC_BIG_ENDIAN);
            offset += 1;
            break;
        case 10: /* From link */
            if (field_type != THRIFT_I32){
                proto_tree_add_expert(attr, pinfo, &ei_rift_thrift_type_err, tvb, offset - THRIFT_FIELD_HEADER_LENGTH, 1);
                return offset;
            }
            proto_tree_add_item(attr, hf_rift_tie_prefix_attr_from_link, tvb, offset, 4, ENC_BIG_ENDIAN);
            offset += 4;
            break;
        default:
            proto_tree_add_expert(attr, pinfo, &ei_rift_thrift_unexpected_field_id, tvb, offset + 1 - THRIFT_FIELD_HEADER_LENGTH, 2);
            return offset;    
        }
        
        field_type = tvb_get_guint8(tvb, offset);
    }

    
    /* Reading struct end */
    offset += 1;

    proto_item_set_end(ai, tvb, offset);
    
    return offset;
}

static int
dissect_prefixes(tvbuff_t *tvb, packet_info *pinfo, proto_tree *parent_tree, int offset)
{
    proto_item *psi, *pei;
    proto_tree *prefixes, *prefix_entry;
    guint key_type, value_type,
        map_size;
        
    /* Getting header of Thrift map */
    key_type = tvb_get_guint8(tvb, offset);
    value_type = tvb_get_guint8(tvb, offset + 1);
    map_size = tvb_get_guint32(tvb, offset + 2, ENC_BIG_ENDIAN);

    offset += THRIFT_MAP_HEADER_LENGTH;

    if (key_type != THRIFT_STRUCT){
        proto_tree_add_expert(parent_tree, pinfo, &ei_rift_thrift_type_err, tvb, offset - THRIFT_MAP_HEADER_LENGTH, 1);
        return offset;
    }

    if (value_type != THRIFT_STRUCT){
        proto_tree_add_expert(parent_tree, pinfo, &ei_rift_thrift_type_err, tvb, offset + 1 - THRIFT_MAP_HEADER_LENGTH, 1);
        return offset;
    }

    /* Creating subtree for neighbors */
    prefixes = proto_tree_add_subtree(parent_tree, tvb, offset, 0, ett_rift_tie_prefixes, &psi,
                                  "Total prefixes:");

    proto_item_append_text(psi, " %u ", map_size);

    col_append_fstr(pinfo->cinfo, COL_INFO, "| %u prefixes sent ", map_size);
    
    while (map_size > 0){

        prefix_entry = proto_tree_add_subtree(prefixes, tvb, offset, 0, ett_rift_tie_prefix_entry, &pei,
                                              "");
        /* Dissecting key */
        offset = dissect_ip_prefix(tvb, pinfo, prefix_entry, pei, offset);

        /* Dissecting value */
        offset = dissect_prefix_attr(tvb, pinfo, prefix_entry, offset);

        proto_item_set_end(pei, tvb, offset);

        map_size -= 1;
    }

    proto_item_set_end(psi, tvb, offset);
    
    return offset;
}

static int 
dissect_tie_prefix_elem(tvbuff_t *tvb, packet_info *pinfo, proto_tree *parent_tree, int offset)
{
    proto_item *ti;
    proto_tree *tree;
    guint field_type, field_id;
   
    tree = proto_tree_add_subtree(parent_tree, tvb, offset, 0, ett_rift_tie_prefix, &ti,
                                                      "Prefix TIE");

    field_type = tvb_get_guint8(tvb, offset);
    field_id = tvb_get_guint16(tvb, offset + 1, ENC_BIG_ENDIAN);
    offset += THRIFT_FIELD_HEADER_LENGTH;

    switch (field_id){
    case 1:
        if (field_type != THRIFT_MAP){
            proto_tree_add_expert(tree, pinfo, &ei_rift_thrift_type_err, tvb, offset - THRIFT_FIELD_HEADER_LENGTH, 1);
            return offset;
        }
        offset = dissect_prefixes(tvb, pinfo, tree, offset);
        break;
    default:
        proto_tree_add_expert(tree, pinfo, &ei_rift_thrift_unexpected_field_id, tvb, offset + 1 - THRIFT_FIELD_HEADER_LENGTH, 1);
        return offset;
    }
    
    /* Reading struct end */
    offset += 1;

    proto_item_set_end(ti, tvb, offset);

    return offset;
}


static int 
dissect_tie_key_value_elem(tvbuff_t *tvb, packet_info *pinfo, proto_tree *parent_tree, int offset)
{ 
    proto_tree *tree, *keyvals, *pair;
    proto_item *ti, *ki, *pi;
    guint field_type, field_id,
        key_type, value_type,
        map_size;
    guint32 key_len, value_len;
    gchar key;

    tree = proto_tree_add_subtree(parent_tree, tvb, offset, 0, ett_rift_tie_key_value, &ti,
                                  "Key Value TIE");

    field_type = tvb_get_guint8(tvb, offset);

    while (field_type != THRIFT_STRUCT_END){
        field_id = tvb_get_guint16(tvb, offset + 1, ENC_BIG_ENDIAN);
        offset += THRIFT_FIELD_HEADER_LENGTH;
        
        switch (field_id){
        case 1:
            if (field_type != THRIFT_MAP){
                proto_tree_add_expert(tree, pinfo, &ei_rift_thrift_type_err, tvb, offset - THRIFT_FIELD_HEADER_LENGTH, 1);
                return offset;
            }

            /* Getting header of Thrift map */
            key_type = tvb_get_guint8(tvb, offset);
            value_type = tvb_get_guint8(tvb, offset + 1);
            map_size = tvb_get_guint32(tvb, offset + 2, ENC_BIG_ENDIAN);

            offset += THRIFT_MAP_HEADER_LENGTH;
    
            if (key_type != THRIFT_STRING){
                proto_tree_add_expert(tree, pinfo, &ei_rift_thrift_type_err, tvb, offset - THRIFT_MAP_HEADER_LENGTH, 1);
                return offset;
            }
    
            if (value_type != THRIFT_STRING){
                proto_tree_add_expert(tree, pinfo, &ei_rift_thrift_type_err, tvb, offset + 1 - THRIFT_FIELD_HEADER_LENGTH, 1);
                return offset;
            }

            /* Creating subtree for key-value pairs */
            keyvals = proto_tree_add_subtree(tree, tvb, offset, 0, ett_rift_tie_key, &ki,
                                               "Total key-value pairs:");

            proto_item_append_text(ki, " %u ", map_size);

            col_append_fstr(pinfo->cinfo, COL_INFO, " %u key-value pairs", map_size);

            /* Iterating through neighbors map */
            while (map_size > 0){

                pair = proto_tree_add_subtree(keyvals, tvb, offset, 0, ett_rift_tie_key_value, &pi,
                                              "");

                /* Getting key length*/
                key_len = tvb_get_guint32(tvb, offset, ENC_BIG_ENDIAN);                
                tvb_get_raw_bytes_as_string(tvb, offset + 4, &key, key_len);

                proto_item_append_text(pi, " %s", &key);
                proto_tree_add_item(pair, hf_rift_tie_key_value_key, tvb, offset + 4, key_len, ENC_BIG_ENDIAN);
                offset += 4 + key_len;

                /* Getting value length*/
                value_len = tvb_get_guint32(tvb, offset, ENC_BIG_ENDIAN);

                proto_tree_add_item(pair, hf_rift_tie_key_value_val, tvb, offset + 4, value_len, ENC_BIG_ENDIAN);
                offset += 4 + value_len;
            
                proto_item_set_end(pi, tvb, offset);

                map_size -= 1;
            }

            /* Setting end of neighbors list in buffer*/
            proto_item_set_end(keyvals, tvb, offset);
            
            break;
        default:
            proto_tree_add_expert(tree, pinfo, &ei_rift_thrift_unexpected_field_id, tvb, offset + 1 - THRIFT_FIELD_HEADER_LENGTH, 1);
            return offset;
        }

        field_type = tvb_get_guint8(tvb, offset);
    }

    /* Reading struct end */
    offset += 1;

    proto_item_set_end(ti, tvb, offset);
    
    return offset;
}


static int
dissect_tie_element(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, int offset)
{

    guint field_type, field_id;

    field_type = tvb_get_guint8(tvb, offset);

    while (field_type != THRIFT_STRUCT_END){
        
        /* Getting field id */
        field_id = tvb_get_guint16(tvb, offset + 1, ENC_BIG_ENDIAN);
        offset += THRIFT_FIELD_HEADER_LENGTH;

        /* Getting type of TIE Packet */
        switch (field_id)
            {
            case 1: /* Node Element */
                if (field_type != THRIFT_STRUCT){
                    proto_tree_add_expert(tree, pinfo, &ei_rift_thrift_type_err, tvb, offset - THRIFT_FIELD_HEADER_LENGTH, 1);
                    return offset;
                }
                offset = dissect_tie_node_elem(tvb, pinfo, tree, offset);
                break;
            case 2: /* Prefix Element*/
                if (field_type != THRIFT_STRUCT){
                    proto_tree_add_expert(tree, pinfo, &ei_rift_thrift_type_err, tvb, offset - THRIFT_FIELD_HEADER_LENGTH, 1);
                    return offset;
                }
                offset = dissect_tie_prefix_elem(tvb, pinfo, tree, offset);
                break;
            case 3: /* Possitive Dissagregation Prefix */
                if (field_type != THRIFT_STRUCT){
                    proto_tree_add_expert(tree, pinfo, &ei_rift_thrift_type_err, tvb, offset - THRIFT_FIELD_HEADER_LENGTH, 1);
                    return offset;
                }
                offset = dissect_tie_prefix_elem(tvb, pinfo, tree, offset);
                break;
            case 5: /* Negative Dissagregation Prefix */
                if (field_type != THRIFT_STRUCT){
                    proto_tree_add_expert(tree, pinfo, &ei_rift_thrift_type_err, tvb, offset - THRIFT_FIELD_HEADER_LENGTH, 1);
                    return offset;
                }
                offset = dissect_tie_prefix_elem(tvb, pinfo, tree, offset);
                break;
            case 6: /* External Prefix */
                if (field_type != THRIFT_STRUCT){
                    proto_tree_add_expert(tree, pinfo, &ei_rift_thrift_type_err, tvb, offset - THRIFT_FIELD_HEADER_LENGTH, 1);
                    return offset;
                }
                offset = dissect_tie_prefix_elem(tvb, pinfo, tree, offset);
                break;
            case 7: /* Positive External Disaggregated Prefix */
                if (field_type != THRIFT_STRUCT){
                    proto_tree_add_expert(tree, pinfo, &ei_rift_thrift_type_err, tvb, offset - THRIFT_FIELD_HEADER_LENGTH, 1);
                    return offset;
                }
                offset = dissect_tie_prefix_elem(tvb, pinfo, tree, offset);
                break;
            case 9: /* Key Value Element */
                if (field_type != THRIFT_STRUCT){
                    proto_tree_add_expert(tree, pinfo, &ei_rift_thrift_type_err, tvb, offset - THRIFT_FIELD_HEADER_LENGTH, 1);
                    return offset;
                }
                offset = dissect_tie_key_value_elem(tvb, pinfo, tree, offset);
                break;
            default:
                proto_tree_add_expert(tree, pinfo, &ei_rift_thrift_unexpected_field_id, tvb, offset + 1 - THRIFT_FIELD_HEADER_LENGTH, 2);
                return offset;
            }

    field_type = tvb_get_guint8(tvb, offset);
    }

    /* Reading struct end */
    offset += 1;

    return offset; 
}

static int 
dissect_tie_packet(tvbuff_t *tvb, packet_info *pinfo, proto_tree *parent_tree, int offset)
{
    
    guint field_type, field_id;
    proto_tree *tree;
    proto_item *ti;

    /* Creating subtree for Security RIFT Header Envelope */      
    tree = proto_tree_add_subtree(parent_tree, tvb, offset, 0, ett_rift_tie, &ti,
                                                      "Topology Information Element (TIE)");

    /*Getting TIE Header*/
    field_type = tvb_get_guint8(tvb, offset);
    offset += 1;

    /* Type should be a struct */
    if (field_type != THRIFT_STRUCT){
        proto_tree_add_expert(tree, pinfo, &ei_rift_thrift_type_err, tvb, offset - 1, 1);
        return offset;
    }

    /* Getting field id */
    field_id = tvb_get_guint16(tvb, offset, ENC_BIG_ENDIAN);

    /* Add Packet type to tree */
    offset += 2;

    if (field_id != 1){
        proto_tree_add_expert(tree, pinfo, &ei_rift_thrift_unexpected_field_id, tvb, offset - 2, 2);
        return offset;
    }

    col_set_str(pinfo->cinfo, COL_INFO, "TIE Message | ");

    offset = dissect_tie_header(tvb, pinfo, tree, offset);

    /*Getting TIE Element*/
    field_type = tvb_get_guint8(tvb, offset);
    offset += 1;

    /* Type should be a struct */
    if (field_type != THRIFT_STRUCT){
        proto_tree_add_expert(tree, pinfo, &ei_rift_thrift_type_err, tvb, offset - 1, 1);
        return offset;
    }

    /* Getting field id */
    field_id = tvb_get_guint16(tvb, offset, ENC_BIG_ENDIAN);

    /* Add Packet type to tree */
    offset += 2;

    if (field_id != 2){
        proto_tree_add_expert(tree, pinfo, &ei_rift_thrift_unexpected_field_id, tvb, offset - 2, 2);
        return offset;
    }

    offset = dissect_tie_element(tvb, pinfo, tree, offset);

    proto_item_set_end(ti, tvb, offset);

    return offset;
}

static int
dissect_tie_header_with_lifetime(tvbuff_t *tvb, packet_info *pinfo, proto_tree *parent_tree, int offset)
{
    proto_item *ti;
    proto_tree *tree;
    guint field_type, field_id;
    guint32 lifetime;
    tree = proto_tree_add_subtree(parent_tree, tvb, offset, 0, ett_rift_tie_header_with_lifetime, &ti,
                                                      "TIE Header with lifetime ");
    field_type = tvb_get_guint8(tvb, offset);

    while (field_type != THRIFT_STRUCT_END){
        field_id = tvb_get_guint16(tvb, offset + 1, ENC_BIG_ENDIAN);
        offset += THRIFT_FIELD_HEADER_LENGTH;

        switch (field_id){
        case 1: /* TIE Header */
            if (field_type != THRIFT_STRUCT){
                proto_tree_add_expert(parent_tree, pinfo, &ei_rift_thrift_type_err, tvb, offset - THRIFT_FIELD_HEADER_LENGTH, 1);
                return offset;
            }
            offset = dissect_tie_header(tvb, pinfo, tree, offset);
            break;
        case 2: /* Remaining lifetime */
            if (field_type != THRIFT_I32){
                proto_tree_add_expert(parent_tree, pinfo, &ei_rift_thrift_type_err, tvb, offset - THRIFT_FIELD_HEADER_LENGTH, 1);
                return offset;
            }

            /* Adding lifetime info to the tree label */
            lifetime = tvb_get_guint32(tvb, offset, ENC_BIG_ENDIAN);
            proto_item_append_text(ti, "%u", lifetime);

            proto_tree_add_item(tree, hf_rift_tie_header_remaining_lifetime, tvb, offset, 4, ENC_BIG_ENDIAN);
            offset += 4;
            break;
        default:
            proto_tree_add_expert(parent_tree, pinfo, &ei_rift_thrift_unexpected_field_id, tvb, offset + 1 - THRIFT_FIELD_HEADER_LENGTH, 2);
            return offset;
        }
        
        field_type = tvb_get_guint8(tvb, offset);
    } 
    
    /* Reading struct end */
    offset += 1;

    proto_item_set_end(ti, tvb, offset);

    return offset;
}

static int 
dissect_tide_packet(tvbuff_t *tvb, packet_info *pinfo,  proto_tree *parent_tree, int offset)
{
    proto_item *ti, *sti;
    proto_tree *tree, *headers_tree;
    guint field_type, field_id;
    guint32 list_size, headers;

    /* Creating subtree for Security RIFT Header Envelope */      
    tree = proto_tree_add_subtree(parent_tree, tvb, offset, 0, ett_rift_tire, &ti,
                                                      "Topology Information Description Element (TIDE)");

    field_type = tvb_get_guint8(tvb, offset);

    while (field_type != THRIFT_STRUCT_END){
        field_id = tvb_get_guint16(tvb, offset + 1, ENC_BIG_ENDIAN);
        offset += THRIFT_FIELD_HEADER_LENGTH;

        switch (field_id){
        case 1: /* Start range */
            if (field_type != THRIFT_STRUCT){
                proto_tree_add_expert(parent_tree, pinfo, &ei_rift_thrift_type_err, tvb, offset - THRIFT_FIELD_HEADER_LENGTH, 1);
                return offset;
            }
            
            headers_tree = proto_tree_add_subtree(tree, tvb, offset, 0, ett_rift_tieid, &sti,
                                   "Start range");
            offset = dissect_tieid(tvb, pinfo, headers_tree, offset);
            proto_item_set_end(sti, tvb, offset);
            break;
        case 2: /* End range */
            if (field_type != THRIFT_STRUCT){
                proto_tree_add_expert(parent_tree, pinfo, &ei_rift_thrift_type_err, tvb, offset - THRIFT_FIELD_HEADER_LENGTH, 1);
                return offset;
            }
            
            headers_tree = proto_tree_add_subtree(tree, tvb, offset, 0, ett_rift_tieid, &sti,
                                   "End range");
            
            offset = dissect_tieid(tvb, pinfo, headers_tree, offset);
            proto_item_set_end(sti, tvb, offset);
            break;
        case 3: /* Headers */
            if (field_type != THRIFT_LIST){
                proto_tree_add_expert(parent_tree, pinfo, &ei_rift_thrift_type_err, tvb, offset - THRIFT_FIELD_HEADER_LENGTH, 1);
                return offset;
            }

            headers_tree = proto_tree_add_subtree(tree, tvb, offset, 0, ett_rift_tieid, &sti,
                                   "List of headers");

            /* Getting list type and size*/
            field_type = tvb_get_guint8(tvb, offset);
            list_size = tvb_get_guint32(tvb, offset + 1, ENC_BIG_ENDIAN);
            offset += THRIFT_SET_HEADER_LENGTH;
            headers = list_size;
    
            /* Dissect every element of list */
            while (list_size > 0){
                offset = dissect_tie_header_with_lifetime(tvb, pinfo, headers_tree, offset);
                list_size -= 1;
            }

            col_add_fstr(pinfo->cinfo, COL_INFO, "TIDE Message | %u TIE Headers", headers);

            proto_item_set_end(sti, tvb, offset);
            
            break;
        default:
            proto_tree_add_expert(parent_tree, pinfo, &ei_rift_thrift_unexpected_field_id, tvb, offset + 1 - THRIFT_FIELD_HEADER_LENGTH, 2);
            return offset;
    }

        field_type = tvb_get_guint8(tvb, offset);
    }

    /* Reading struct end */
    offset += 1;

    proto_item_set_end(ti, tvb, offset);
    
    return offset;
}

static int 
dissect_tire_packet(tvbuff_t *tvb, packet_info *pinfo, proto_tree *parent_tree, int offset)
{
    proto_item *ti;
    proto_tree *tree;
    guint field_type, field_id;
    guint32 set_size, headers;

    /* Creating subtree for Security RIFT Header Envelope */      
    tree = proto_tree_add_subtree(parent_tree, tvb, offset, 0, ett_rift_tire, &ti,
                                                      "Topology Information Request Element (TIRE)");

    field_type = tvb_get_guint8(tvb, offset);
    offset += 1;

    if (field_type != THRIFT_SET){
        proto_tree_add_expert(parent_tree, pinfo, &ei_rift_thrift_type_err, tvb, offset - 1, 1);
        return offset;
    }

    field_id = tvb_get_guint16(tvb, offset, ENC_BIG_ENDIAN);
    offset += 2;

    if (field_id != 1){
        proto_tree_add_expert(parent_tree, pinfo, &ei_rift_thrift_unexpected_field_id, tvb, offset - 2, 2);
        return offset;
    }
            
    /* Getting set type and size*/
    field_type = tvb_get_guint8(tvb, offset);
    set_size = tvb_get_guint32(tvb, offset + 1, ENC_BIG_ENDIAN);
    offset += THRIFT_SET_HEADER_LENGTH;

    headers = set_size;
    
    /* Dissect every element of set */
    while (set_size > 0){
        offset = dissect_tie_header_with_lifetime(tvb, pinfo, tree, offset);
        set_size -= 1;
    }
    
    col_add_fstr(pinfo->cinfo, COL_INFO, "TIRE Message | %u TIE Headers", headers);

    proto_item_set_end(ti, tvb, offset);

    /* Reading struct end */
    offset += 1;
    
    return offset;
}

static int dissect_neighbor (tvbuff_t *tvb, packet_info *pinfo, proto_tree *parent_tree, int offset)
{
    proto_tree *tree;
    proto_item *ti;
    guint field_type, field_id;

    tree = proto_tree_add_subtree(parent_tree, tvb, offset, 0, ett_rift_tie_link_id_pair, &ti,
                                  "Neighbor");

    field_type = tvb_get_guint8(tvb, offset);

    while (field_type != THRIFT_STRUCT_END){
        field_id = tvb_get_guint16(tvb, offset + 1, ENC_BIG_ENDIAN);
        offset += THRIFT_FIELD_HEADER_LENGTH;

        switch (field_id){
        case 1: /* Originator */
            if (field_type != THRIFT_I64){
                    proto_tree_add_expert(tree, pinfo, &ei_rift_thrift_type_err, tvb, offset - THRIFT_FIELD_HEADER_LENGTH, 1);
                    return offset;
                }
            
            proto_tree_add_item(tree, hf_rift_lie_originator, tvb, offset, 8, ENC_BIG_ENDIAN);
            offset += 8;
            break;
        case 2: /* Remote ID */
            if (field_type != THRIFT_I32){
                proto_tree_add_expert(tree, pinfo, &ei_rift_thrift_type_err, tvb, offset - THRIFT_FIELD_HEADER_LENGTH, 1);
                return offset;
            }
            
            proto_tree_add_item(tree, hf_rift_remote_link_id_type, tvb, offset, 4, ENC_BIG_ENDIAN);
            offset += 4;
            break;
        default: /* Unexpected value */
            proto_tree_add_expert(tree, pinfo, &ei_rift_thrift_unexpected_field_id, tvb, offset + 1 - THRIFT_FIELD_HEADER_LENGTH, 2);
            return offset;                
        }
        
        field_type = tvb_get_guint8(tvb, offset);    
    }
    
    /* Reading struct end */
    offset += 1;

    proto_item_set_end(ti, tvb, offset);
    
    return offset;
}

static int dissect_link_capabilities(tvbuff_t *tvb, packet_info *pinfo, proto_tree *parent_tree, int offset)
{
    proto_tree *tree;
    proto_item *ti;
    guint field_type, field_id;

    tree = proto_tree_add_subtree(parent_tree, tvb, offset, 0, ett_rift_link_cap, &ti,
                                  "Link capabilities");

    field_type = tvb_get_guint8(tvb, offset);

    while (field_type != THRIFT_STRUCT_END){
        field_id = tvb_get_guint16(tvb, offset + 1, ENC_BIG_ENDIAN);
        offset += THRIFT_FIELD_HEADER_LENGTH;

        switch (field_id){
        case 1: /* BFD */
            if (field_type != THRIFT_BOOL){
                proto_tree_add_expert(tree, pinfo, &ei_rift_thrift_unexpected_field_id, tvb, offset - THRIFT_FIELD_HEADER_LENGTH, 1);
                return offset;
            }

            proto_tree_add_item(tree, hf_rift_link_cap_bfd, tvb, offset, 1, ENC_BIG_ENDIAN);
            offset += 1;
            break;
        case 2: /* v4 forwarding capable */
            if (field_type != THRIFT_BOOL){
                proto_tree_add_expert(tree, pinfo, &ei_rift_thrift_unexpected_field_id, tvb, offset - THRIFT_FIELD_HEADER_LENGTH, 1);
                return offset;
            }

            proto_tree_add_item(tree, hf_rift_link_cap_v4_fwd, tvb, offset, 1, ENC_BIG_ENDIAN);
            offset += 1;
            break;
        default: /* Unexpected value */
            proto_tree_add_expert(tree, pinfo, &ei_rift_thrift_unexpected_field_id, tvb, offset + 1 - THRIFT_FIELD_HEADER_LENGTH, 2);
            return offset;
        }

        field_type = tvb_get_guint8(tvb, offset);
    }

    /* Reading struct end */
    offset += 1;

    proto_item_set_end(ti, tvb, offset);
    
    return offset;
}

static int 
dissect_lie_packet(tvbuff_t *tvb, packet_info *pinfo, proto_tree *parent_tree, int offset)
{
    proto_tree *tree;
    proto_item *ti;
    guint field_type, field_id;
    guint16 flood_port;
    guint32 name_len, inst_name_len,
        local_id;

    /* Creating subtree for Security RIFT Header Envelope */      
    tree = proto_tree_add_subtree(parent_tree, tvb, offset, 0, ett_rift_lie, &ti,
                                                      "Link Information Element (LIE)");

    col_set_str(pinfo->cinfo, COL_INFO, "LIE Message |");

    field_type = tvb_get_guint8(tvb, offset);

    while (field_type != THRIFT_STRUCT_END){
        field_id = tvb_get_guint16(tvb, offset + 1, ENC_BIG_ENDIAN);
        offset += THRIFT_FIELD_HEADER_LENGTH;

        switch (field_id){
        case 1: /* Name */
            if (field_type != THRIFT_STRING){
                proto_tree_add_expert(tree, pinfo, &ei_rift_thrift_type_err, tvb, offset - THRIFT_FIELD_HEADER_LENGTH, 1);
                return offset;
            }

            /* Reading string length */
            name_len = tvb_get_guint32(tvb, offset, ENC_BIG_ENDIAN);
                    
            proto_tree_add_item(tree, hf_rift_lie_name, tvb, offset + 4, name_len, ENC_BIG_ENDIAN);
            offset += (4 + name_len);
            break;
        case 2: /* Local link ID */
            if (field_type != THRIFT_I32){
                proto_tree_add_expert(tree, pinfo, &ei_rift_thrift_type_err, tvb, offset - THRIFT_FIELD_HEADER_LENGTH, 1);
                return offset;
            }
            
            local_id = tvb_get_guint32(tvb, offset, ENC_BIG_ENDIAN);
            col_append_fstr(pinfo->cinfo, COL_INFO, " Local ID: %u", local_id);

            proto_tree_add_item(tree, hf_rift_local_link_id_type, tvb, offset, 4, ENC_BIG_ENDIAN);
            offset += 4;
            break;
        case 3: /* Flood port */
            if (field_type != THRIFT_I16){
                proto_tree_add_expert(tree, pinfo, &ei_rift_thrift_type_err, tvb, offset - THRIFT_FIELD_HEADER_LENGTH, 1);
                return offset;
            }

            flood_port = tvb_get_guint16(tvb, offset, ENC_BIG_ENDIAN);
            col_append_fstr(pinfo->cinfo, COL_INFO, " Flood port: %u", flood_port);

            proto_tree_add_item(tree, hf_rift_lie_flood_port, tvb, offset, 2, ENC_BIG_ENDIAN);
            offset += 2;            
            break;
        case 4: /* Link MTU size */
            if (field_type != THRIFT_I32){
                proto_tree_add_expert(tree, pinfo, &ei_rift_thrift_type_err, tvb, offset - THRIFT_FIELD_HEADER_LENGTH, 1);
                return offset;
            }

            proto_tree_add_item(tree, hf_rift_lie_mtu_size, tvb, offset, 4, ENC_BIG_ENDIAN);
            offset += 4;
            break;
        case 5: /* Link bandwidth */
            if (field_type != THRIFT_I32){
                proto_tree_add_expert(tree, pinfo, &ei_rift_thrift_type_err, tvb, offset - THRIFT_FIELD_HEADER_LENGTH, 1);
                return offset;
            }

            proto_tree_add_item(tree, hf_rift_lie_link_bandwidth, tvb, offset, 4, ENC_BIG_ENDIAN);
            offset += 4;
            break;
        case 6: /* Neighbor */
            if (field_type != THRIFT_STRUCT){
                proto_tree_add_expert(tree, pinfo, &ei_rift_thrift_type_err, tvb, offset - THRIFT_FIELD_HEADER_LENGTH, 1);
                return offset;
            }
            
            offset = dissect_neighbor(tvb, pinfo, tree, offset); 
            break;
        case 7: /* PoD */
            if (field_type != THRIFT_I32){
                proto_tree_add_expert(tree, pinfo, &ei_rift_thrift_type_err, tvb, offset - THRIFT_FIELD_HEADER_LENGTH, 1);
                return offset;
            }

            proto_tree_add_item(tree, hf_rift_lie_node_pod, tvb, offset, 4, ENC_BIG_ENDIAN);
            offset += 4;
            break;
        case 10: /* Node capabilities */
            if (field_type != THRIFT_STRUCT){
                proto_tree_add_expert(tree, pinfo, &ei_rift_thrift_type_err, tvb, offset - THRIFT_FIELD_HEADER_LENGTH, 1);
                return offset;
            }
            
            offset = dissect_node_capabilities(tvb, pinfo, tree, offset);
            break;
        case 11: /* Link capabilities */
            if (field_type != THRIFT_STRUCT){
                proto_tree_add_expert(tree, pinfo, &ei_rift_thrift_type_err, tvb, offset - THRIFT_FIELD_HEADER_LENGTH, 1);
                return offset;
            }

            offset = dissect_link_capabilities(tvb, pinfo, tree, offset);
            break;
        case 12: /* Holdtime */
            if (field_type != THRIFT_I16){
                proto_tree_add_expert(tree, pinfo, &ei_rift_thrift_type_err, tvb, offset - THRIFT_FIELD_HEADER_LENGTH, 1);
                return offset;
            }

            proto_tree_add_item(tree, hf_rift_lie_adj_holdtime, tvb, offset, 2, ENC_BIG_ENDIAN);
            offset += 2;
            break;
        case 13: /* Label */
            if (field_type != THRIFT_I32){
                proto_tree_add_expert(tree, pinfo, &ei_rift_thrift_type_err, tvb, offset - THRIFT_FIELD_HEADER_LENGTH, 1);
                return offset;
            }

            proto_tree_add_item(tree, hf_rift_lie_label_type, tvb, offset, 4, ENC_BIG_ENDIAN);
            offset += 4;
            break;
        case 21: /* Not a ZTP offer */
            if (field_type != THRIFT_BOOL){
                proto_tree_add_expert(tree, pinfo, &ei_rift_thrift_type_err, tvb, offset - THRIFT_FIELD_HEADER_LENGTH, 1);
                return offset;
            }

            proto_tree_add_item(tree, hf_rift_lie_not_ztp_offer, tvb, offset, 1, ENC_BIG_ENDIAN);
            offset += 1;
            break;
        case 22: /* You are flood repeater */
            if (field_type != THRIFT_BOOL){
                proto_tree_add_expert(tree, pinfo, &ei_rift_thrift_type_err, tvb, offset - THRIFT_FIELD_HEADER_LENGTH, 1);
                return offset;
            }

            proto_tree_add_item(tree, hf_rift_lie_flood_repeater, tvb, offset, 1, ENC_BIG_ENDIAN);
            offset += 1;
            break;
        case 23: /* You are sending too quickly */
            if (field_type != THRIFT_BOOL){
                proto_tree_add_expert(tree, pinfo, &ei_rift_thrift_type_err, tvb, offset - THRIFT_FIELD_HEADER_LENGTH, 1);
                return offset;
            }

            proto_tree_add_item(tree, hf_rift_lie_sending_too_quickly, tvb, offset, 1, ENC_BIG_ENDIAN);
            offset += 1;
            break;
        case 24: /* Instance name */
            if (field_type != THRIFT_STRING){
                proto_tree_add_expert(tree, pinfo, &ei_rift_thrift_type_err, tvb, offset - THRIFT_FIELD_HEADER_LENGTH, 1);
                return offset;
            }

            /* Reading string length */
            inst_name_len = tvb_get_guint32(tvb, offset, ENC_BIG_ENDIAN);
                    
            proto_tree_add_item(tree, hf_rift_lie_inst_name, tvb, offset + 4, inst_name_len, ENC_BIG_ENDIAN);
            offset += (4 + inst_name_len);            
            break;
        default: /* Unexpected value */
            proto_tree_add_expert(tree, pinfo, &ei_rift_thrift_unexpected_field_id, tvb, offset + 1 - THRIFT_FIELD_HEADER_LENGTH, 2);
            return offset;
        }

        field_type = tvb_get_guint8(tvb, offset);
    }

    /* Reading struct end */
    offset += 1;

    proto_item_set_end(ti, tvb, offset);
    
    return offset;
}

static int
dissect_packet_content(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, int offset)
{

    guint field_type, field_id;
    /*Getting type field of Packet Content*/
    field_type = tvb_get_guint8(tvb, offset);
    offset += 1;

    /* Type should be a struct */
    if (field_type != THRIFT_STRUCT){
        proto_tree_add_expert(tree, pinfo, &ei_rift_thrift_type_err, tvb, offset - 1, 1);
        return offset;
    }
    
    /* Getting field id */
    field_id = tvb_get_guint16(tvb, offset, ENC_BIG_ENDIAN);
    

    /* Add Packet type to tree */
    proto_tree_add_item(tree, hf_rift_pkt_type, tvb, offset, 2, ENC_BIG_ENDIAN);
    offset += 2;

    /* Call dissector for type of RIFT packet */
    switch(field_id)
        {
        case 1: /* LIE Packet */
            offset = dissect_lie_packet(tvb, pinfo, tree, offset);
            break;
        case 2: /* TIDE Packet */ 
            offset = dissect_tide_packet(tvb, pinfo, tree, offset);
            break;
        case 3: /* TIRE Packet */
            offset = dissect_tire_packet(tvb, pinfo, tree, offset);
            break;
        case 4: /* TIE Packet */
            offset = dissect_tie_packet(tvb, pinfo, tree, offset);
            break;
        default: /* Unexpected value */
            proto_tree_add_expert(tree, pinfo, &ei_rift_thrift_unexpected_field_id, tvb, offset - 2, 2);
            return offset;     
        }


    /* Reading union end */
    offset += 1;
        

    return offset;
}


/* Dissect Serialized RIFT Model Object */
static int
dissect_serialized_rift_model_obj(tvbuff_t *tvb, packet_info *pinfo, proto_tree *parent_tree,
                                  int offset)
{
    proto_item *ti;
    proto_tree *tree;
    guint field_type, field_id;

    /* Creating subtree for Serialized RIFT Model Object */      
    tree = proto_tree_add_subtree(parent_tree, tvb, offset, 0, ett_rift_serialized_obj, &ti,
                                                      "Serialized RIFT Model Object");
    
    /* Getting first byte inside ProtocolPacket,
     * which should indicate Thrift field type
     */
    field_type = tvb_get_guint8(tvb, offset);
    offset += 1;

    /* Type should be a struct */
    if (field_type != THRIFT_STRUCT){
        proto_tree_add_expert(tree, pinfo, &ei_rift_thrift_type_err, tvb, offset - 1, 1);
        return offset;
    }

    /* Getting field id */
    field_id = tvb_get_guint16(tvb, offset, ENC_BIG_ENDIAN);
    offset += 2;

    /* Packet Header must go first */
    if (field_id != 1){
        proto_tree_add_expert(tree, pinfo, &ei_rift_thrift_unexpected_field_id, tvb, offset - 2, 2);
        return offset;
    }
    
    offset = dissect_packet_header(tvb, pinfo, tree, offset); /* PacketHeader */
    
    field_type = tvb_get_guint8(tvb, offset);
    offset += 1;

    /* Type should be a struct */
    if (field_type != THRIFT_STRUCT){
        proto_tree_add_expert(tree, pinfo, &ei_rift_thrift_type_err, tvb, offset - 1, 1);
        return offset;
    }
    
    /* Getting field id */
    field_id = tvb_get_guint16(tvb, offset, ENC_BIG_ENDIAN);
    offset += 2;

    if (field_id != 2){
        proto_tree_add_expert(tree, pinfo, &ei_rift_thrift_unexpected_field_id, tvb, offset - 2, 2);
        return offset;
    }
    
    offset = dissect_packet_content(tvb, pinfo, tree, offset); /* PacketContent */

    /* Reading struct end */
    offset += 1;
    
    proto_item_set_end(ti, tvb, offset);
    
    return offset; 
}

static gboolean
test_rift(tvbuff_t *tvb, packet_info *pinfo _U_, proto_tree *tree _U_, void *data _U_)
{
    /*** HEURISTICS ***/

    /* Check that there's enough data */
    if (tvb_captured_length(tvb) < RIFT_MIN_LENGTH)
        return FALSE;

    /*If first first two bytes are not equal to RIFT Magic value*/
    if (tvb_get_guint16(tvb, 0, ENC_BIG_ENDIAN) != RIFT_MAGIC)
        return FALSE;

    /* Assuming it's our packet */
    return TRUE;
}

/* Code to actually dissect the packets */
static int
dissect_rift(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree,
        void *data _U_)
{
    /* Set up structures needed to add the protocol subtree and manage it */
    proto_item *ti;
    proto_tree *rift_tree;

    /* Other misc. local variables. */
    guint offset = 0;
    
    /* Set the Protocol column to the constant string of RIFT */
    col_set_str(pinfo->cinfo, COL_PROTOCOL, "RIFT");

    col_set_str(pinfo->cinfo, COL_INFO, "RIFT Message");

    /* create display subtree for the protocol */
    ti = proto_tree_add_item(tree, proto_rift, tvb, 0, -1, ENC_NA);
    rift_tree = proto_item_add_subtree(ti, ett_rift);

    /* Call dissector for Outer Security Envelope Header*/
    offset = dissect_outer_sec_env_header(tvb, rift_tree, offset);

    /* Call dissector for Serialized RIFT Model Object */
    offset = dissect_serialized_rift_model_obj(tvb, pinfo, rift_tree, offset);

    /* Reading struct end */
    offset += 1;

    /* Return the amount of data this dissector was able to dissect (which may
     * or may not be the entire packet as we return here). */
    return tvb_captured_length(tvb);
}

static gboolean
dissect_rift_heur(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, void *data)
{
    conversation_t *conversation;
    
    if (!test_rift(tvb, pinfo, 0, data))
        return FALSE;

    conversation = find_or_create_conversation(pinfo);
    conversation_set_dissector(conversation, rift_handle);

    /*   and do the dissection */
    dissect_rift(tvb, pinfo, tree, data);

    return TRUE;
}

/* Register the protocol with Wireshark.
 *
 * This format is require because a script is used to build the C function that
 * calls all the protocol registration.
 */
void
proto_register_rift(void)
{   
    /* Setup list of header fields */
    static hf_register_info hf[] = {
        
        /* Outer Security Envelope Header */
        { &hf_rift_magic,
          { "RIFT Magic", "rift.magic",
            FT_UINT16, BASE_HEX, NULL, 0x0,
            "Constant value of 0xA1F7 that allows to classify RIFT packets independent of used UDP port.",
            HFILL }},
        {  &hf_rift_pkt_number,
           {"Packet Number", "rift.pkt_number",
            FT_UINT16, BASE_DEC, NULL, 0x0,
            "An optional, per packet type monotonically growing number rolling over using sequence number arithmetic.",
            HFILL}},
        {  &hf_rift_reserved,
           {"Reserved", "rift.reserved",
            FT_NONE, BASE_NONE, NULL, 0x0,
            "Reserved byte.", HFILL}},
        {  &hf_rift_major_version,
           {"RIFT Major Version", "rift.major_version",
            FT_UINT8, BASE_DEC, NULL, 0x0,
            "It allows to check whether protocol versions are compatible, i.e. if the serialized object can be decoded at all.",
            HFILL}},
        {  &hf_rift_outer_key_id,
            {"Outer Key ID", "rift.outer_key_id",
            FT_UINT8, BASE_DEC, NULL, 0x0, 
            "This implies key type and algorithm. Value 0 means that no valid fingerprint was computed.", HFILL}},
        { &hf_rift_fingerprint_len, 
            {"Security Fingerprint Length", "rift.fingerprint_length",
            FT_UINT8, BASE_DEC, NULL, 0x0, 
            "Length in 32-bit multiples of the following fingerprint.", HFILL}},
        { &hf_rift_fingerprint,
            {"Security Fingerprint",  "rift.fingerprint",
            FT_BYTES, BASE_NONE, NULL, 0x0,  
            "This is a signature that is computed over all data following after it.", HFILL}},
        { &hf_rift_weak_nonce_local, 
            {"Weak Nonce Local", "rift.weak_nonce_local",
            FT_UINT16, BASE_DEC, NULL, 0x0, 
            "Weak Nonce of the adjacency as advertised in LIEs.", HFILL}},
        { &hf_rift_weak_nonce_remote, 
            {"Weak Nonce Remote", "rift.weak_nonce_remote",
            FT_UINT16, BASE_DEC, NULL, 0x0, 
            "Remote Weak Nonce of the adjacency as advertised in LIEs.", HFILL}},
        { &hf_rift_tie_ttl, 
            {"TIE Time-To-Live", "rift_tie_ttl",
            FT_UINT32, BASE_DEC, NULL, 0x0, 
            "For TIEs this field represents the remaining lifetime of the TIE and Origin Security Envelope Header MUST be present in the packet.",
             HFILL}},
        { &hf_rift_tie_origin_key_id,
          {"TIE Origin Key ID", "rift.tie_origin_key_id",
           FT_UINT24, BASE_DEC, NULL, 0x0,
           "This implies key type and used algorithm. Value 0 means that no valid fingerprint was computed.",
           HFILL}},
        { &hf_rift_tie_fingerprint_len,
          {"TIE Security Fingerprint Length", "rift_tie_fingerprint_len",
           FT_UINT8, BASE_DEC, NULL, 0x0,
           "Length in 32-bit multiples of the following fingerprint.",
           HFILL}},
         { &hf_rift_tie_fingerprint,
            {"TIE Security Fingerprint",  "rift.fingerprint",
            FT_BYTES, BASE_NONE, NULL, 0x0,
             "This is a signature that is computed over all data following after it.", HFILL}},

        /* Serialized RIFT Model Object */

        /* Packet Header */
        { &hf_rift_ser_major_version,
          {"Protocol major version", "rift.ser_major_version",
           FT_UINT8, BASE_DEC, NULL, 0x0,
           "Major version type of protocol.", HFILL}},
        { &hf_rift_ser_minor_version,
          {"Protocol minor version", "rift.ser_minor_version",
           FT_UINT16, BASE_DEC, NULL, 0x0,
           "Minor version type of protocol.", HFILL}},
        { &hf_rift_ser_sender,
          {"Sender", "rift.ser_sender",
           FT_UINT64, BASE_DEC, NULL, 0x0,
           "Node sending the packet, in case of LIE/TIRE/TIDE also the originator of it.", HFILL}},
        { &hf_rift_ser_lvl,
          {"Level", "rift.ser_level",
           FT_UINT8, BASE_DEC, NULL, 0x0,
           "Level of the node sending the packet, required on everything except LIEs. Lack of presence on LIEs indicates UNDEFINED_LEVEL and is used in ZTP procedures.",
           HFILL}},
        
        /* Packet Content */

        { &hf_rift_pkt_type,
          {"Packet Type", "rift.pkt_type",
           FT_UINT16, BASE_DEC, VALS(riftpkttypevals), 0x0,
           "RIFT Packet Type", HFILL}},
       
        /* TIE Packet */
      
        { &hf_rift_tie_originator, 
            {"Packet Originator", "rift.tie_originator", 
            FT_UINT64, BASE_DEC, NULL, 0x0, 
            "TIE originator", HFILL}},

        { &hf_rift_tie_number,
            {"TIE Number", "rift.tie_number", 
            FT_UINT32, BASE_DEC, NULL, 0x0, 
            "Rift TIE Number", HFILL}},

        { & hf_rift_tie_id_seqnum, 
            {"Sequence Number", "rift.tie_seqnum",
            FT_UINT64, BASE_DEC, NULL, 0x0,
            "TIE Sequence Number", HFILL}},    
        
        { &hf_rift_tie_elem_type, 
            {"TIE Element Type", "rift.tie_elem_type", 
             FT_UINT32, BASE_DEC, VALS(tieelemtypevals), 0x0,
            "RIFT TIE Element Type", HFILL}},

        { &hf_rift_tie_direction, 
            {"Direction", "rift.tie_direction",
             FT_UINT32, BASE_DEC, VALS(tiedirectionvals), 0x0, 
            "RIFT TIE Direction", HFILL}},

        { &hf_rift_ieee802_timestamp_sec,
            { "Originator time AS sec", "rift.tie_assec", 
            FT_UINT64, BASE_DEC, NULL, 0x0, 
            "TIE Originator time AS nsec", HFILL}}, 

        { &hf_rift_ieee802_timestamp_nsec,
            { "Originator time AS nsec", "rift.tie_asnsec", 
            FT_UINT32, BASE_DEC, NULL, 0x0, 
            "TIE Originator time AS nsec", HFILL}}, 

        { &hf_rift_tie_lifetime,
          { "Origination Lifetime", "rift.tie_lifetime", 
            FT_UINT32, BASE_DEC, NULL, 0x0, 
            "TIE Origination Lifetime", HFILL}}, 
        
        { &hf_rift_tie_node_level,
          {"Node Level", "rift.tie_node_level",
           FT_UINT8, BASE_DEC, NULL, 0x0, 
           "Level of the node", HFILL}},

        { &hf_rift_node_capabilities_minor_version,
          {"Minor version", "rift.node_capabilities_minor_version",
           FT_UINT16, BASE_DEC, NULL, 0x0,
           "Supported minor version.", HFILL}},
       
        { &hf_rift_node_capabilities_flood_reduction,
          {"Flood reduction", "rift.node_capabilities_flood_reduction",
           FT_BOOLEAN, BASE_NONE, NULL, 0x0,
           "Can this node participate in flood reduction?", HFILL}},

        { &hf_rift_node_capabilities_hierarchy_indications,
          {"Hierarchy indications", "rift.node_capabilities_hierarchy_indications",
           FT_UINT32, BASE_DEC, VALS(hierarchyindicationsvals), 0x0,
           "Does this node restrict itself to be top-of-fabric or leaf only (in ZTP) and does it support leaf-2-leaf procedures?", HFILL}},

        { &hf_rift_node_flag_overload,
          {"Node Flags", "rift.tie_node_flag_overload",
           FT_BOOLEAN, BASE_NONE, NULL, 0x0,
           "Flags of the node", HFILL}},
        
        {&hf_rift_tie_node_name,
         {"Node Name", "rift.tie_node_name",
          FT_STRING, STR_UNICODE, NULL, 0x0,
          "Optional node name for easier operations.", HFILL}},

        {&hf_rift_tie_node_pod,
         {"Node PoD", "rift_tie_node_pod",
          FT_UINT32, BASE_DEC, NULL, 0x0,
          "PoD to which the node belongs.", HFILL}},

        {&hf_rift_miscabled_link_id_type,
         {"Miscabled Link ID", "rift.miscabled_link_id_type",
          FT_UINT32, BASE_DEC, NULL, 0x0,
          "ID for a miscabled link.", HFILL}},
          

        /* Link ID Pair */

        {&hf_rift_local_link_id_type,
         {"Local Link ID", "rift.local_link_id_type",
          FT_UINT32, BASE_DEC, NULL, 0x0,
          "Node-wide unique value for a local link.", HFILL}},

        {&hf_rift_remote_link_id_type,
         {"Remote Link ID", "rift.remote_link_id_type",
          FT_UINT32, BASE_DEC, NULL, 0x0,
          "Remote link ID for a link.", HFILL}},

        {&hf_rift_platform_if_index,
         {"Platform Interface index", "rift.platform_if_index",
          FT_UINT32, BASE_DEC, NULL, 0x0,
          "Platform Interface Index type, i.e. index of interface on hardware, can be used e.g. with RFC5837",
          HFILL}},

        {&hf_rift_platform_if_name,
         {"Platform Interface name", "rift.platform_if_name",
          FT_STRING, STR_UNICODE, NULL, 0x0,
          "Describes the local interface name.", HFILL}},

        {&hf_rift_link_id_sec_key,
         {"Trusted Outer Security Key", "rift.link_id_sec_key",
          FT_UINT8, BASE_DEC, NULL, 0x0,
          "Indication whether the link is secured, i.e. protected by outer key, absence of this element means no indication, undefined outer key means not secured",
          HFILL}},

        {&hf_rift_bfd_up,
         {"BFD Session up", "rift.bfd_up",
          FT_BOOLEAN, BASE_NONE, NULL, 0x0,
          "Indication whether the link is protected by established BFD session.", HFILL}},

        /* Node Neigbors TIE Element */
        {&hf_rift_node_neighbor_id,
         {"Neighbor ID", "rift.node_neighbor_id",
          FT_UINT64, BASE_DEC, NULL, 0x0,
          "ID of a neighbor.", HFILL}},

        {&hf_rift_node_neighbor_level,
         {"Neighbor Level", "rift.node_neighbor_level",
          FT_UINT8, BASE_DEC, NULL, 0x0,
          "Level of neighbor.", HFILL}},
        
        {&hf_rift_node_neighbor_cost,
         {"Neighbor Cost", "rift.node_neighbor_cost",
          FT_UINT32, BASE_DEC, NULL, 0x0,
          "Cost to neighbor.", HFILL}},

        {&hf_rift_node_neighbor_bandwidth,
         {"Bandwidth", "rift.node_neighbor_bandwidth",
          FT_UINT32, BASE_DEC, NULL, 0x0,
          "Total bandwith to neighbor, this will be normally sum of the bandwidths of all the parallel links.",
          HFILL}},

        /* Prefix TIE Element*/
        { &hf_rift_tie_prefix_ipv4,
          {"IPv4 Address", "rift.tie_prefix_ipv4",
           FT_IPv4, BASE_NONE, NULL, 0x0,
           "IPv4 address of a prefix.", HFILL}},

        { &hf_rift_tie_prefix_ipv6,
          {"IPv6 Address", "rift.tie_prefix_ipv6",
           FT_IPv6, BASE_NONE, NULL, 0x0,
           "IPv6 address of a prefix.", HFILL}},

        { &hf_rift_tie_prefix_len,
          {"Prefix length", "rift.tie_prefix_len",
           FT_UINT8, BASE_DEC, NULL, 0x0,
           "Length of a prefix.", HFILL}},

        /* Prefix Attributes */

        { &hf_rift_tie_prefix_attr_metric,
                {"Metric", "rift.tie_prefix_attr_metric",
                        FT_UINT32, BASE_DEC, NULL, 0x0,
                        "Distance of the prefix.", HFILL}},

        { &hf_rift_tie_prefix_attr_route_tag,
          {"Route tag", "rift.tie_prefix_attr_route_tag",
           FT_UINT64, BASE_DEC, NULL, 0x0,
           "Route tag. Can be redistributed to other protocols or use within the context of real time analytics.",
           HFILL}},
        
        { &hf_rift_tie_prefix_attr_trans_id,
          {"Prefix transaction ID", "rift.tie_prefix_attr_trans_id",
           FT_UINT8, BASE_DEC, NULL, 0x0,
           "Transaction ID type for prefix mobility as specified by RFC6550.", HFILL}},

        { &hf_rift_tie_prefix_attr_loopback,
          {"Loopback", "rift.tie_prefix_attr_loopback",
           FT_BOOLEAN, BASE_NONE, NULL, 0x0,
           "Indicates if the interface is a node loopback.", HFILL}},

        { &hf_rift_tie_prefix_attr_dir_attached,
          {"Directly attached", "rift.tie_prefix_attr_dir_attached",
           FT_BOOLEAN, BASE_NONE, NULL, 0x0,
           "Indicates that the prefix is directly attached, i.e. should be routed to even if the node is in overload.",
           HFILL}},

        { &hf_rift_tie_prefix_attr_from_link,
          {"From link", "rift.tie_prefix_attr_from_link",
           FT_UINT32, BASE_DEC, NULL, 0x0,
           "In case of locally originated prefixes, i.e. interface addresses this candescribe which link the address belongs to.",
           HFILL}},

        /* Key-Value TIE Element */

        { &hf_rift_tie_key_value_key,
          {"Key ID", "rift.tie_key_value_key",
           FT_STRING, STR_UNICODE, NULL, 0x0,
           "Key Value key ID type.", HFILL}},

        { &hf_rift_tie_key_value_val,
          {"Value", "rift.tie_key_value_val",
           FT_STRING, STR_UNICODE, NULL, 0x0,
           "Key Value value type", HFILL}},

        /* TIRE/TIDE Packet */

        { &hf_rift_tie_header_remaining_lifetime,
          {"Remaining lifetime", "rift.tie_header_remaining_lifetime",
           FT_UINT32, BASE_DEC, NULL, 0x0,
           "Remaining lifetime that expires down to 0 just like in ISIS.",
           HFILL}},

        /* LIE Packet */

        { &hf_rift_lie_name,
          {"Name", "rift.lie_name",
           FT_STRING, STR_UNICODE, NULL, 0x0,
           "Node or adjacency name.", HFILL}},

        { &hf_rift_lie_flood_port,
          {"Flood port", "rift.lie_flood_port",
           FT_UINT16, BASE_DEC, NULL, 0x0,
           "UDP port to which we can receive flooded TIEs.",
           HFILL}},

        { &hf_rift_lie_mtu_size,
          {"Link MTU Size", "rift.lie_mtu_size",
           FT_UINT32, BASE_DEC, NULL, 0x0,
           "Layer 3 MTU, used to discover to mismatch.",
           HFILL}},
        
        {&hf_rift_lie_link_bandwidth,
         {"Link bandwidth", "rift.lie_link_bandwidth",
          FT_UINT32, BASE_DEC, NULL, 0x0,
          "Local link bandwidth on the interface.",
          HFILL}},

        { &hf_rift_lie_originator, 
            {"Packet Originator", "rift.lie_originator", 
            FT_UINT64, BASE_DEC, NULL, 0x0, 
            "LIE originator", HFILL}},

        {&hf_rift_lie_node_pod,
         {"Node PoD", "rift_lie_node_pod",
          FT_UINT32, BASE_DEC, NULL, 0x0,
          "PoD to which the node belongs.", HFILL}},

        { &hf_rift_lie_adj_holdtime,
          {"Holdtime", "rift.lie_adj_holdtime",
           FT_UINT16, BASE_DEC, NULL, 0x0,
           "Required holdtime of the adjacency, i.e. how much time MUST expire without LIE for the adjacency to drop.",
           HFILL}},

        {&hf_rift_lie_label_type,
         {"Label", "rift_lie_label_type",
          FT_UINT32, BASE_DEC, NULL, 0x0,
          "Unsolicited, downstream assigned locally significant label value for the adjacency.",
          HFILL}},

        {&hf_rift_lie_not_ztp_offer,
         {"Not a ZTP offer", "rift.lie_not_ztp_offer",
          FT_BOOLEAN, BASE_NONE, NULL, 0x0,
          "Indicates that the level on the LIE MUST NOT be used to derive a ZTP level by the receiving node.",
          HFILL}},

        {&hf_rift_lie_flood_repeater,
         {"You are flood repeater", "rift.lie_flood_repeater",
          FT_BOOLEAN, BASE_NONE, NULL, 0x0,
          "Indicates to northbound neighbor that it should be reflooding this node's N-TIEs to achieve flood reduction and balancing for northbound flooding.",
          HFILL}},

        {&hf_rift_lie_sending_too_quickly,
         {"You are sending too quickly", "rift.lie_sending_too_quickly",
          FT_BOOLEAN, BASE_NONE, NULL, 0x0,
          "Can be optionally set to indicate to neighbor that packet losses are seen on reception based on packet numbers or the rate is too high.",
          HFILL}},

        {&hf_rift_lie_inst_name,
         {"Instance name", "rift.lie_inst_name",
          FT_STRING, STR_UNICODE, NULL, 0x0,
          "Instance name in case multiple RIFT instances running on same interface.", HFILL}},

        /* Link capabilities */
        
        {&hf_rift_link_cap_bfd,
         {"BFD", "rift.link_cap_bfd",
          FT_BOOLEAN, BASE_NONE, NULL, 0x0,
          "Indicates that the link is supporting BFD.", HFILL}},

        {&hf_rift_link_cap_v4_fwd,
         {"v4 forwarding", "rift.link_cap_v4_fwd",
          FT_BOOLEAN, BASE_NONE, NULL, 0x0,
          "Indicates whether the interface will support v4 forwarding.", HFILL}}

    };

    /* Setup protocol subtree array */
    static gint *ett[] = {
        &ett_rift,
        &ett_rift_sec_header,
        &ett_rift_tie_sec_header,
        &ett_rift_serialized_obj, 
        &ett_rift_lie,
        &ett_rift_tire,
        &ett_rift_tide, 
        &ett_rift_tie,
        &ett_rift_tie_node,
        &ett_rift_tie_prefix,
        &ett_rift_tie_possitive_diss,
        &ett_rift_tie_negative_diss,
        &ett_rift_tie_external,
        &ett_rift_tie_ext_possitive_diss,
        &ett_rift_tie_key_value,
        &ett_rift_tie_key,
        &ett_rift_tie_neighbors,
        &ett_rift_tie_neighbor,
        &ett_rift_tie_link_id_pairs,
        &ett_rift_tie_link_id_pair,
        &ett_rift_tie_miscabled_link_id,
        &ett_rift_tie_prefixes,
        &ett_rift_tie_prefix_entry,
        &ett_rift_tie_prefix_attr,
        &ett_rift_tie_prefix_attr_mon_clock,
        &ett_rift_tie_prefix_attr_route_tags,
        &ett_rift_tie_header_with_lifetime,
        &ett_rift_tieid,
        &ett_rift_lie_neighbor,
        &ett_rift_node_cap,
        &ett_rift_link_cap
    };

    static ei_register_info ei[] = {
        { &ei_rift_thrift_type_err,
          { "rift.thrift_type_err", PI_PROTOCOL, PI_ERROR,
            "Unexpected Thrift type", EXPFILL }},
        { &ei_rift_thrift_unexpected_field_id,
          { "rift.thrift_unexpected_field_id", PI_PROTOCOL, PI_ERROR,
            "Unexpected field id value according to current version", EXPFILL }},
        { &ei_rift_tie_dir_err,
          { "rift.tie_dir_err", PI_UNDECODED, PI_ERROR,
            "Error decoding TIE Direction field", EXPFILL }},
        { &ei_rift_tie_elem_err,
          { "rift.tie_elem_err", PI_UNDECODED, PI_ERROR,
            "Error decoding TIE Element type field", EXPFILL }}
    };

    expert_module_t* expert_rift;

    /* Register the protocol name and description */
    proto_rift = proto_register_protocol("Routing in Fat Trees",
            "RIFT", "rift");

    /* Required function calls to register the header fields and subtrees */
    proto_register_field_array(proto_rift, hf, array_length(hf));
    proto_register_subtree_array(ett, array_length(ett));

    expert_rift = expert_register_protocol(proto_rift);
    expert_register_field_array(expert_rift, ei, array_length(ei));

}

void
proto_reg_handoff_rift(void)
{
    rift_handle = create_dissector_handle(dissect_rift,
                                          proto_rift);

    /* register as heuristic dissector for UDP */
    heur_dissector_add("udp", dissect_rift_heur, "RIFT over UDP",
                       "rift_udp", proto_rift, HEURISTIC_ENABLE);
    dissector_add_for_decode_as_with_preference("udp.port", rift_handle);
}

/*
 * Editor modelines  -  http://www.wireshark.org/tools/modelines.html
 *
 * Local variables:
 * c-basic-offset: 4
 * tab-width: 8
 * indent-tabs-mode: nil
 * End:
 *
 * vi: set shiftwidth=4 tabstop=8 expandtab:
 * :indentSize=4:tabSize=8:noTabs=true:
 */
