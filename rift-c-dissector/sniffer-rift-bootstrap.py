import pyshark
import os
import sys
import argparse
from datetime import datetime

    
def main(args):

    count = [0]*4
    window = None
    capture = pyshark.LiveCapture(interface='any', display_filter='rift')

    for packet in capture.sniff_continuously():
        if packet.highest_layer == "RIFT":
            pkt_type = packet['rift'].get_field_value('pkt_type')

            if pkt_type == '1':
                if window is not None:
                    window += 1
                count[0] += 1
            elif pkt_type == '2':
                if window is not None:
                    window += 1
                count[1] += 1
            elif pkt_type == '3':
                if window is not None:
                    window += 1
                count[2] += 1
            elif pkt_type == '4':
                window = 0
                count[3] += 1
            else:
                print("Pkt_type desconocido")
                return

            if window != None and window == 1000:
                now = datetime.now()
                dt_string = now.strftime("%d-%m-%Y_%H-%M")
                with open(os.path.join('/shared', args.topo  + '.csv'), 'a') as f:
                    f.write(args.node_name + "," + dt_string +  ',' +  str(count[0]) + "," + str(count[1]) + "," 
                        + str(count[2]) + "," + str(count[3]) + "\n")
                print("LIES: " + str(count[0]) + "\nTIDES: " + str(count[1]) + "\nTIRES: " 
                        + str(count[2]) + "\nTIES: " + str(count[3]) + "\n")
                f.close
                return

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('node_name',  help="Name of Node.")
    parser.add_argument('topo', help="Used Topology")
    args = parser.parse_args()
    main(args)
