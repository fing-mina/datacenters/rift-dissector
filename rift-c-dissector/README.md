# RIFT C dissector
RIFT C dissector is a complete Wireshark dissector for decoding RIFT protocol.

The code identifies all the RIFT packets and builds a complete dissection for the TIE ones.

The dissector has been tested with the current implementations of RIFT [2] [3].

# Setup:

0. Install additional dependencies (or CMake can fail):
```
sudo apt-get install libglib2.0-dev libgcrypt20-dev flex bison qtbase5-dev qttools5-dev qtmultimedia5-dev libqt5svg5-dev
```

1.  Compile Wireshark 3.2.2 from source code

**Note**: Uninstall previous versions of Wireshark. Do not install plugin yet.

```
a.  Download Wireshark 3.2.2 from https://2.na.dl.wireshark.org/src/wireshark-3.2.2.tar.xz
b.  In the folder where wireshark is located do:
    $ mkdir build && cd build     -> Make new directory for build files and position there
    $ cmake ../  -> Configures dependencies and enviroment
    $ make                        
    $ sudo make install           ->  Installs Wireshark
c.  Run wireshark and check that it works
```

2.  Compile plugin "rift"

```
a.  Make new folder under $wireshark_dir$/plugins/epan/rift where $wireshark_dir$ is the folder of point 1b
b.  Copy files packet-rift.c, CMakeLists.txt, AUTHORS, Readme.md and other source files related in new folder.
c.  Modify file $wireshark_dir$/CMakeListsCustom.txt.example, rename to CMakeListsCustom.txt and edit line 16 of file this way:
    # Fail CMake stage if any of these plugins are missing from source tree 
    set(CUSTOM_PLUGIN_SRC_DIR
    #	private_plugins/foo
    # or
        plugins/epan/rift
    )
d.  Repeat 1.b) now with plugin, it will change configuration files, remake and install.

Note: If 1.b) change is ignored, you may comment line 21 (#plugins/epan/bar) of CMakeListsCustom.txt.  

e. Open Wireshark and check with RIFT capture if it works.

Note: If configuration is changed (new folder, new plugin or rename) you have to repeat 2.d) again. If there are changes in source code just do make / sudo make install to apply changes. 
```

# Setup References
https://blog.fjh1997.top/2019/03/29/show-you-how-to-compile-the-c-language-plugin-for-wireshark-3.1-step-by-step-(windows-platform-2019-3-20)/
 
https://www.wireshark.org/docs/wsug_html_chunked/ChBuildInstallUnixBuild.html

# Dissector
The dissector is in file packet-rift.c, this file includes Wireshark API calls and it will make calls to local functions. 

At the moment we have managed to dissect the Security Envelope based on [1]. 

- Security Envelope: Outer Security Envelope Header and the TIE Origin Security Envelope Header. 
- Serialized RIFT Model Object: In process, based on Thrift model used in rift implementation.

# RIFT Packet

      UDP Header:
      +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
      |           Source Port         |       RIFT destination port   |
      +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
      |           UDP Length          |        UDP Checksum           |
      +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+

      Outer Security Envelope Header:
      +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
      |           RIFT MAGIC          |         Packet Number         |
      +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
      |    Reserved   |  RIFT Major   | Outer Key ID  | Fingerprint   |
      |               |    Version    |               |    Length     |
      +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
      |                                                               |
      ~       Security Fingerprint covers all following content       ~
      |                                                               |
      +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
      | Weak Nonce Local              | Weak Nonce Remote             |
      +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
      |            Remaining TIE Lifetime (all 1s in case of LIE)     |
      +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+

      TIE Origin Security Envelope Header:
      +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
      |              TIE Origin Key ID                |  Fingerprint  |
      |                                               |    Length     |
      +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
      |                                                               |
      ~       Security Fingerprint covers all following content       ~
      |                                                               |
      +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+

      Serialized RIFT Model Object
      +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
      |                                                               |
      ~                Serialized RIFT Model Object                   ~
      |                                                               |
      +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+

# Pending work

Testing of Key Value TIE Elements is pending. 

# References
[1] RIFT: Routing in Fat Trees (I-D): https://datatracker.ietf.org/doc/draft-ietf-rift-rift/

[2] RIFT Python by Bruno Rijsman: https://github.com/brunorijsman/rift-python

[3] RIFT Juniper Trial: https://www.juniper.net/us/en/dm/free-rift-trial/

