**RIFT dissector**

The RIFT dissector project has as purpose the dissection of RIFT packets in Wireshark.
(https://www.wireshark.org/)

We developed a partial dissector in Lua scripting language for the RIFT Security Envelope and a complete C dissector for  RIFT packets.

See rift-c-dissector directory for the complete C dissector or rift-lua-dissector for the Lua development.

This project is part of a collaboration between the University of the Republic (UdelaR) and Roma Tre university.


